Kuinka teet uuden scenen prefabeilla:
CANVAKSEN LIS�YS
-Raahaa sceneen kanvas Prefab-kansiosta. T�ll� hetkell� vain yksi canvas nimelt��n "KiinteatTilitCanvas"
-Mik�li unity ei luo sceneen canvaksen lis��misen j�lkeen "EventSystem"-objektia on sinun lis�tt�v� se sinne itse. Hiiren oikea hierarkiassa->UI->EventSystem
-Lis�� �sken sceneen lis��m��si canvaksella olevaan "GameLogicScript":iin viittaukset inspectorissa mik�li ne eiv�t ole paikallaan
	-->Info Teksti:iin canvaksen lapsena oleva "infoTeksti"
	-->Kuvaus Teksti:iin canvaksen lapsena oleva "TehtavaKuvausTeksti"
	-->Vaiheita yht ja kirjauksia tehty tapahtumaan t�yttyv�t itsest��n pelin k�ynnistyess�
SCENECREATOR
-Lis�� sceness� olevaan kameraan SceneCreatorScript
-Tarkista t�st� scriptist� viittaukset "Tili prefab" sek� "Canvas"
	-->Tili prefab l�ytyy Prefab-kansiosta, "Tili"
	-->Canvakseen voi vet�� sceness� olevan "KiinteatTilitCanvas"-objektin
	-->"Tilien Paikat Canvaksella"-kohtaa tulee tehd� viittaukset kaikkiin sceness� olevan canvaksen "Tilipaikka1"-objekteihin
-Hyv� tiet�� SceneCreatorScriptist�
	-nelj� ylint� valintaruutua("Vah Yks Tili") ohittavat tarvittaessa "Tileja yht"-sliderilla s��dett�v�n arvon. Esim. jos olet valinnut sliderilla vain 2 tili�, mutta kaikki 4 tickboxia on painettuna niin luodaan yksi jokaista tilityyppi�.
        -"Luo randomist tulo tileja" sek� "Luo randomisti paaoma tileja" ovat aika itsest��n kuvaavia.
	-Mik�li peliss� ei ole tulo- tai p��omatilej� niin peliss� tulee pelaajalle eteen vain menokirjauksia

T. Janne