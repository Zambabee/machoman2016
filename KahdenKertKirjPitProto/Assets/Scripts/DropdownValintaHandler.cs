﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class DropdownValintaHandler : MonoBehaviour {

    public TiliPaikanToiminnotScript paikanToiminnotScript;
    public GameObject perusLabel;
    public string perusLabelTeksti;
    [Range(0,1)]
    public int tiliryhma;
    public GameObject[] tilienPaikat;
    public GameObject tilienPaikkaPiilossa;
    public tilinTyyppi naidenTilienTyyppi;
    [SerializeField]
    private List<int> tamanDropDowninTilienIdt = new List<int>();
    private Dropdown drop;
    private int edellisenValitunTilinId = 0; //Muuttuja jota käytetään kun siirretään näkyvissä oleva tili pois näkyvistä

    

	// Use this for initialization
	IEnumerator Start () {
        drop = GetComponent<Dropdown>();

        yield return new WaitForEndOfFrame();
        GetComponent<Dropdown>().options.Clear();
        perusLabel.GetComponent<Text>().text = perusLabelTeksti;
        Debug.Log("Menotilejä: " + SceneCreatorScript.instance.menoTilit.Count);
        Debug.Log("Rahatilejä: " + SceneCreatorScript.instance.rahaTilit.Count);
        Debug.Log("Tulotilejä: " + SceneCreatorScript.instance.tuloTilit.Count);
        Debug.Log("Pääomatilejä: " + SceneCreatorScript.instance.paaomaTilit.Count);
        

       

        

        switch (naidenTilienTyyppi) { 
            case tilinTyyppi.menotili:
                if (SceneCreatorScript.instance.menoTilit.Count == 0)
                {
                    gameObject.SetActive(false);
                    Debug.Log("Mentiin sulkemaan tiliä");
                }
                else {
                    for (int i = 0; i < SceneCreatorScript.instance.menoTilit.Count; i++)
                    {
                        Dropdown.OptionData option = new Dropdown.OptionData();
                        option.text = SceneCreatorScript.instance.menoTilit[i].GetComponent<TilinScript>().tilinNimiEnum.ToString();
                        GetComponent<Dropdown>().options.Add(option);
                        tamanDropDowninTilienIdt.Add(SceneCreatorScript.instance.menoTilit[i].GetComponent<TilinScript>().tilinId);
                    }
                }
                break;
            case tilinTyyppi.tulotili:
                if (SceneCreatorScript.instance.tuloTilit.Count == 0)
                {
                    gameObject.SetActive(false);
                }
                else
                {
                    for (int i = 0; i < SceneCreatorScript.instance.tuloTilit.Count; i++)
                    {
                        Dropdown.OptionData option = new Dropdown.OptionData();
                        option.text = SceneCreatorScript.instance.tuloTilit[i].GetComponent<TilinScript>().tilinNimiEnum.ToString();
                        GetComponent<Dropdown>().options.Add(option);
                        tamanDropDowninTilienIdt.Add(SceneCreatorScript.instance.tuloTilit[i].GetComponent<TilinScript>().tilinId);
                    }
                }
                break;
            case tilinTyyppi.rahatili:
                if (SceneCreatorScript.instance.rahaTilit.Count == 0)
                {
                    gameObject.SetActive(false);
                }
                else {
                    for (int i = 0; i < SceneCreatorScript.instance.rahaTilit.Count; i++)
                    {
                        Dropdown.OptionData option = new Dropdown.OptionData();
                        option.text = SceneCreatorScript.instance.rahaTilit[i].GetComponent<TilinScript>().tilinNimiEnum.ToString();
                        GetComponent<Dropdown>().options.Add(option);
                        tamanDropDowninTilienIdt.Add(SceneCreatorScript.instance.rahaTilit[i].GetComponent<TilinScript>().tilinId);
                    }
                }
                break;
            case tilinTyyppi.paaomatili:
                if (SceneCreatorScript.instance.paaomaTilit.Count == 0)
                {
                    gameObject.SetActive(false);
                }
                else
                {
                    for (int i = 0; i < SceneCreatorScript.instance.paaomaTilit.Count; i++)
                    {
                        Dropdown.OptionData option = new Dropdown.OptionData();
                        option.text = SceneCreatorScript.instance.paaomaTilit[i].GetComponent<TilinScript>().tilinNimiEnum.ToString();
                        GetComponent<Dropdown>().options.Add(option);
                        tamanDropDowninTilienIdt.Add(SceneCreatorScript.instance.paaomaTilit[i].GetComponent<TilinScript>().tilinId);
                    }
                }
                break;
        }
	} //Startin loppu

    void Update() { 
        
    }

    void OnMouseDown()
    {
        Debug.Log("Klicked element");
    }

    /*public void KerroOptionsinSisältö() {
        Debug.Log("KerroOptionsinsSisältö kutsuttu");
        GameObject[] kaikkiTilit = GameObject.FindGameObjectsWithTag("Tili");
        //Käytetään paikanToiminntoScriptin metodia viemään edellinen tili pois näkyvistä mikäli jokin tili on nkäyvissä
        if (tiliryhma == 0 && paikanToiminnotScript.PaikallaOlevaTili1)
        {
            paikanToiminnotScript.VieTiliPiiloon(0);
        }
        else if(tiliryhma == 1 && paikanToiminnotScript.PaikallaOlevaTili2){
            paikanToiminnotScript.VieTiliPiiloon(1);
        }

        int id = tamanDropDowninTilienIdt[drop.value];
        edellisenValitunTilinId = id;
        Debug.Log("Valitun tilin id oli: "  + id);
        
        //Siirretään tili jonka id vastaa valinnan id:tä tämän tiliryhmän tilin kohdalle ja kerrotaan samalla paikanToiminnotScriptille mikä on näytettävä tili
        for (int i = 0; i < kaikkiTilit.Length; i++)
        {
            if (kaikkiTilit[i].GetComponent<TilinScript>().tilinId == id) {
                if (tiliryhma == 0) {
                    //kaikkiTilit[i].transform.position = tilienPaikat[0].transform.position;
                    paikanToiminnotScript.TuoTiliEsille(0, kaikkiTilit[i]);
                }
                else if (tiliryhma == 1) {
                    //kaikkiTilit[i].transform.position = tilienPaikat[1].transform.position;
                    paikanToiminnotScript.TuoTiliEsille(1, kaikkiTilit[i]);
                }
            }
        }

        //Muutetaan valintalaatikon teksti takaisin otsikkotasolle
        perusLabel.GetComponent<Text>().text = perusLabelTeksti;
        
    }*/

    public void TuoTiliNakyviin() {    
        Debug.Log("Testen. Dropdown index: " + drop.value);
        StartCoroutine(VaihdaTiliaIe());
    }

    IEnumerator VaihdaTiliaIe() {
        yield return new WaitForSeconds(0.1f);
        GameObject[] kaikkiTilit = GameObject.FindGameObjectsWithTag("Tili");
        //Käytetään paikanToiminntoScriptin metodia viemään edellinen tili pois näkyvistä mikäli jokin tili on nkäyvissä
        if (tiliryhma == 0 && paikanToiminnotScript.PaikallaOlevaTili1)
        {
            paikanToiminnotScript.VieTiliPiiloon(0);
        }
        else if (tiliryhma == 1 && paikanToiminnotScript.PaikallaOlevaTili2)
        {
            paikanToiminnotScript.VieTiliPiiloon(1);
        }

        int id = tamanDropDowninTilienIdt[drop.value];
        edellisenValitunTilinId = id;
        Debug.Log("Valitun tilin id oli: " + id);

        //Siirretään tili jonka id vastaa valinnan id:tä tämän tiliryhmän tilin kohdalle ja kerrotaan samalla paikanToiminnotScriptille mikä on näytettävä tili
        for (int i = 0; i < kaikkiTilit.Length; i++)
        {
            if (kaikkiTilit[i].GetComponent<TilinScript>().tilinId == id)
            {
                if (tiliryhma == 0)
                {
                    //kaikkiTilit[i].transform.position = tilienPaikat[0].transform.position;
                    paikanToiminnotScript.TuoTiliEsille(0, kaikkiTilit[i]);
                }
                else if (tiliryhma == 1)
                {
                    //kaikkiTilit[i].transform.position = tilienPaikat[1].transform.position;
                    paikanToiminnotScript.TuoTiliEsille(1, kaikkiTilit[i]);
                }
            }
        }

        //Muutetaan valintalaatikon teksti takaisin otsikkotasolle
        perusLabel.GetComponent<Text>().text = perusLabelTeksti;
    }



}
