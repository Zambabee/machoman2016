﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CustomTietojenTayttoScript : MonoBehaviour {

    public Button aloitusNappi;
    public Dropdown esitysTapaDrop;
    public InputField kirjaustenSumma;
    public InputField kirjaustenMaara;
    public InputField tilienMaara;
    public Toggle luoTulo;
    public Toggle luoPaaoma;

    public Canvas canvasKiinteina;
    public Canvas canvasListana;
    public GameObject mainCamera;
    public SceneCreatorScript sceneScriptKiintea;

	// Use this for initialization
	void Start () {
        if (esitysTapaDrop.value == 0)
        {
            tilienMaara.GetComponentInChildren<Text>().text = "2-8";
        }
        else {
            tilienMaara.GetComponentInChildren<Text>().text = "2-20";
        }
        TarkistaVoikoAloittaa();
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    public void TarkistaTilienMaaranOikeellisuus() {
        StartCoroutine(tarkistaSyotettyTiliMaara());
    }

    IEnumerator tarkistaSyotettyTiliMaara() {
        yield return new WaitForEndOfFrame();
        if (esitysTapaDrop.value == 0)
        {
            if (tilienMaara.GetComponentsInChildren<Text>()[1].text != "")
            {
                if (int.Parse(tilienMaara.GetComponentsInChildren<Text>()[1].text) > 8)
                {
                    tilienMaara.text = "8";
                }
                else if (int.Parse(tilienMaara.GetComponentsInChildren<Text>()[1].text) < 1)
                {
                    tilienMaara.text = "2";
                }
                Debug.Log("Nyt oli kiinteinä Value inserted is: " + tilienMaara.GetComponentsInChildren<Text>()[1].text);
            }
            else
            {
                if (esitysTapaDrop.value == 0)
                {
                    tilienMaara.GetComponentInChildren<Text>().text = "2-8";
                }
                else
                {
                    tilienMaara.GetComponentInChildren<Text>().text = "2-30";
                }
            }
        }
        else
        {
            if (tilienMaara.GetComponentsInChildren<Text>()[1].text != "")
            {
                Debug.Log("Parsittu arvo on: " + int.Parse(tilienMaara.GetComponentsInChildren<Text>()[1].text));
                if (int.Parse(tilienMaara.GetComponentsInChildren<Text>()[1].text) > 30)
                {
                    tilienMaara.text = "30";
                }
                else if (int.Parse(tilienMaara.GetComponentsInChildren<Text>()[1].text) < 1)
                {
                    tilienMaara.text = "2";
                }
            }
            else {
                if (esitysTapaDrop.value == 0)
                {
                    tilienMaara.GetComponentInChildren<Text>().text = "2-8";
                }
                else
                {
                    tilienMaara.GetComponentInChildren<Text>().text = "2-30";
                }
            }
            Debug.Log("Nyt oli listana ja Value inserted is: " + tilienMaara.GetComponentsInChildren<Text>()[1].text);
        }
        TarkistaVoikoAloittaa();
    }

    public void TarkistaKirjauksienMaara() {
        StartCoroutine(TarkistaKirjauksetKPL());
    }

    IEnumerator TarkistaKirjauksetKPL() {
        yield return new WaitForEndOfFrame();
        if (kirjaustenMaara.text != "")
        {
            if (int.Parse(kirjaustenMaara.text) < 4) {
                kirjaustenMaara.text = "4";
            }
            else if (int.Parse(kirjaustenMaara.text) > 30) {
                kirjaustenMaara.text = "30";
            }
        }
        else {
            kirjaustenMaara.GetComponentInChildren<Text>().text = "4-30";
        }
    }


    public void TarkistaVoikoAloittaa() {
        if (kirjaustenSumma.text != "" && tilienMaara.text != "" && kirjaustenMaara.text != "")
        {
            aloitusNappi.interactable = true;
        }
        else {
            aloitusNappi.interactable = false;
        }
    }

    public void Aloita() {
        if (esitysTapaDrop.value == 0)
        {
            
            if (luoTulo.isOn) {
                sceneScriptKiintea.luoRandomistiTuloTileja = true;
            }
            if (luoPaaoma.isOn) {
                sceneScriptKiintea.luoRandomistiPaaomaTileja = true;
            }
            sceneScriptKiintea.canvas = canvasKiinteina.gameObject;
            sceneScriptKiintea.kuinkaTilitNaytetaan = tilienNayttoTapa.kiinteina;
            sceneScriptKiintea.kirjauksienYhteisArvo = int.Parse(kirjaustenSumma.text);
            sceneScriptKiintea.liiketapahtumiaKpl = int.Parse(kirjaustenMaara.text);
            sceneScriptKiintea.tilejaYht = int.Parse(tilienMaara.text);
            sceneScriptKiintea.enabled = true;
            //sceneScriptKiintea.gameObject.SetActive(true);
            canvasKiinteina.gameObject.SetActive(true);
            gameObject.SetActive(false);
            
        }
        else {
            if (luoTulo.isOn)
            {
                sceneScriptKiintea.luoRandomistiTuloTileja = true;
            }
            if (luoPaaoma.isOn)
            {
                sceneScriptKiintea.luoRandomistiPaaomaTileja = true;
            }
            sceneScriptKiintea.canvas = canvasListana.gameObject;
            sceneScriptKiintea.kuinkaTilitNaytetaan = tilienNayttoTapa.dropdown;
            sceneScriptKiintea.kirjauksienYhteisArvo = int.Parse(kirjaustenSumma.text);
            sceneScriptKiintea.liiketapahtumiaKpl = int.Parse(kirjaustenMaara.text);
            sceneScriptKiintea.tilejaYht = int.Parse(tilienMaara.text);
            sceneScriptKiintea.enabled = true;
            canvasListana.gameObject.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}
