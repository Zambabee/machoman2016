﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public enum tilienNayttoTapa {kiinteina, dropdown }
public enum tilinTyyppi {menotili, tulotili,rahatili,paaomatili }
public enum kirjauksenTyyppi {debit, kredit }
public enum tilinNimi {pankkitili, pankkitilikaytto, pankkitilitalletus, kassatili, ostot, palkat, sähkömenot, mainosmenot, vuokramenot, siivoustarvikkeet, korkomenot, verot, toimistotarvikkeet, ajoneuvomenot, myyntitili, vuokratulot, korkotulot, osinkotulot, yksityiskäyttö, lainat, omapääoma, ostovelat}
public enum tehtavienKuvausVaikeusTaso { helppo,vaikea}

[System.Serializable]
public class Kirjaus {
    public int kirjausTilinId;
    public float kirjauksenArvo;
    public kirjauksenTyyppi tyyppi;
}

[System.Serializable]
public class LiikeTapahtuma {
    public Kirjaus debitKirjaus;
    public Kirjaus kreditKirjaus;
    public string tapahtumaKuvaus;

}

public class SceneCreatorScript : MonoBehaviour {
    //Tämä ompi singletonni
    public static SceneCreatorScript instance;

    public tehtavienKuvausVaikeusTaso vaikeustaso;
    public tilienNayttoTapa kuinkaTilitNaytetaan;
    public float pisteKerroin = 1.0f;
    public float pisteetOikeastaVastauksesta = 100.0f;
    public int liiketapahtumiaKpl;
    public float kirjauksienYhteisArvo;
    [Range(2,22)]
    public int tilejaYht;
    public bool luoVahYksMenoTili = false;
    public bool luoVahYksTuloTili = false;
    public bool luoVahYksRahaTili = false;
    public bool luoVahYksPaaomaTili = false;
    //public bool luoRandomistiMenoTileja = false;
    public bool luoRandomistiTuloTileja = false;
    //public bool luoRandomistiRahaTileja = false;
    public bool luoRandomistiPaaomaTileja = false;
    public GameObject tiliPrefab;
    public List<GameObject> rahaTilit;
    public List<GameObject> menoTilit;
    public List<GameObject> tuloTilit;
    public List<GameObject> paaomaTilit;
    public List<tilinNimi> rahaTilienNimet;
    public List<tilinNimi> menoTilienNimet;
    public List<tilinNimi> tuloTilienNimet;
    public List<tilinNimi> paaomaTilienNimet;
    public GameObject[] tilienPaikatCanvaksellaJosTilitKiinteina;
    public GameObject tilienSpawnObjectJosDropDown;
    public GameObject canvas;

    [SerializeField]
    private List<Kirjaus> kirjaukset = new List<Kirjaus>();
    [SerializeField]
    private List<LiikeTapahtuma> liikeTapahtumat = new List<LiikeTapahtuma>();
    private int pakollisiaTilejaYht = 0; //Muuttuja jota käytetään tileja luotaessa tarkistaessa mitä tilejä pitää luoda
    private List<int> randomTilienDictionaryAvaimet = new List<int>(); //Muuttuja jota käytetään myöhemmin luotaessa randomtileä. Käytetään randomnumeron generoimiseen. Arvo muutetaan Start():ssa
    private Dictionary<int, tilinTyyppi> randomTiliDictionary = new Dictionary<int, tilinTyyppi>();//Dictionary jota käytetään random tilejä tehdessä. Tästä tarkistetaan random int luvulla lukua vastaava tilityyppi. En tiedä onko paras ratkaisu kun pitkään meni tämän käyttöä pähkäillessä.

    public List<LiikeTapahtuma> LiikeTapahtumat
    {
        get { return liikeTapahtumat; }
        set { liikeTapahtumat = value; }
    }

    void Awake() { 
    //Tarkistetaan löytyykö scenestä jo scenecreator, jos löytyy niin tuhotaan tämä, jos ei löydy niin instance on tämä scripti
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else {
            instance = this;
        }
        //tyhjennetään ensin dictionary virheiden välttämiseksi
        randomTiliDictionary.Clear();
        //Lisätään randomTiliDictionaryyn avainparit
        randomTiliDictionary.Add(0,tilinTyyppi.menotili);
        randomTiliDictionary.Add(1, tilinTyyppi.tulotili);
        randomTiliDictionary.Add(2, tilinTyyppi.rahatili);
        randomTiliDictionary.Add(3,tilinTyyppi.paaomatili);
    }

	// Use this for initialization
	void Start () {
        GameObject.FindGameObjectWithTag("PisteText").GetComponent<Text>().text = "Pisteesi: " +  PisteenLaskuJaAchievementManager.instance.nykyisetPisteet;
        //Tarkistetaan tilienluonti boolit ja muutetaan tileaYht-muuttujan arvo boolien mukaiseksi. Hieman raaka toteutustapa ainakin toistaiseksi
        if (kuinkaTilitNaytetaan == tilienNayttoTapa.kiinteina && tilejaYht > 8) { //jos tilit näytetään kiinteinä mutta inspectorista on laitettu tilin määräksi isompi kuin 8 niin lukitaan määrä kahdeksaan
            tilejaYht = 8;
        }
            if (luoVahYksMenoTili) { 
                pakollisiaTilejaYht++;    
            }
            if (luoVahYksTuloTili) {
                pakollisiaTilejaYht++;
            }
            if (luoVahYksRahaTili) {
                pakollisiaTilejaYht++;
            }
            if (luoVahYksPaaomaTili) {
                pakollisiaTilejaYht++;
            }

        //Tarkistetaan mitä kaikkia tilejä voidaan luoda randomisti ja tallennetaan tämä tieto listaan int-arvona. Tätä listassa olevaa int-arvoa voidaan sitten käyttää etsimään tilin tyyppi dictionarystä
        //Samalla tässä syntyvän listan pituutta voidaan käyttää random tilin numeron generoinnissa
        //Meno =0, tulo = 1, raha = 2, paaoma = 3
           /* if (luoRandomistiMenoTileja) {
                randomTilienDictionaryAvaimet.Add(0);
            }
            if (luoRandomistiRahaTileja)
            {
                randomTilienDictionaryAvaimet.Add(2);
            }*/
            randomTilienDictionaryAvaimet.Add(0);
            randomTilienDictionaryAvaimet.Add(2);
            if (luoRandomistiTuloTileja) {
                randomTilienDictionaryAvaimet.Add(1);
            }
           
            if (luoRandomistiPaaomaTileja) {
                randomTilienDictionaryAvaimet.Add(3);
            }
            

        //Tilien määrä tarkastettu, luodaan scene metodilla BuildScene()
        BuildScene();

        
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void BuildScene() {

        int looppienMaara = tilejaYht; //Muuttuja jota käytetään tilien määrän päättämisessä
        if (pakollisiaTilejaYht > tilejaYht) {
            looppienMaara = pakollisiaTilejaYht;
            Debug.Log("Pakollisia tileja oli enemmän kuin tilejaYht muuttujan arvo. Tilejä tehdään: " + looppienMaara + " kappaletta");
        }

        for (int i = 0; i < looppienMaara; i++) //For jossa tehdään kaikki tilit randomisti parametrien mukaan
        {
            //tehdään ensin tarkistus onko tilienimiä ylipäätään jäljellä käytettäväksi. Jos ei ole niin skipataan tilin luonti
            //käytetään apuna muuttujaa johon otetaan talteen kaikki jäljellä olevat tilien nimet määrällisesti
            int nimiaJaljella = rahaTilienNimet.Count + menoTilienNimet.Count + tuloTilienNimet.Count + paaomaTilienNimet.Count;
            if (nimiaJaljella > 0) //jos nimiä on vielä jäljellä niin käydään luomassa tili, muuten skipataan aina tilin luonti. Näin ei synny virheitä
            {
                GameObject tili;
                if (kuinkaTilitNaytetaan == tilienNayttoTapa.kiinteina)
                {
                    tili = Instantiate(tiliPrefab, tilienPaikatCanvaksellaJosTilitKiinteina[i].transform.position, Quaternion.identity) as GameObject; //Luodaan tili ja tallennetaan se muuttujaan tili. Joka kierroksella luodaan siis klooni prefabista joka toimii kentällä omana yksikkönään
                }
                else
                {
                    tili = Instantiate(tiliPrefab, tilienSpawnObjectJosDropDown.transform.position, Quaternion.identity) as GameObject; //Luodaan tili ja tallennetaan se muuttujaan tili. Joka kierroksella luodaan siis klooni prefabista joka toimii kentällä omana yksikkönään
                }
                tili.transform.SetParent(canvas.transform); //Asetetaan tilin vanhemmaksi canvas peliobjekti
                tili.transform.localScale = new Vector3(3.0f,3.0f,3.0f);
                //Seuraavaksi hirveä ifelse hässäkkä. Käytännössä tarkistaa eka onko jäljellä tehtäviä pakollisia tileja. Pakollisien tilien jälkeen tarkistaa onko tehtynä vähintään 2 erilaista tiliä pelaamisen mahdollistamiseksi
                if (luoVahYksRahaTili && rahaTilienNimet.Count > 0)
                {
                    luoVahYksRahaTili = false;
                    //Asetetaan tilinnimienumiksi rahatiliennimistä ensimmäinen eli toivottavasti pankkitili

                    tili.GetComponent<TilinScript>().tilinNimiEnum = rahaTilienNimet[0];
                    tili.GetComponent<TilinScript>().tilinNimi = "Rahatili";
                    tili.GetComponent<TilinScript>().tyyppi = tilinTyyppi.rahatili;
                    tili.GetComponent<TilinScript>().AlustaTilinTiedot();
                    rahaTilit.Add(tili);
                    //poistetaan käytetty tilinnimi listasta
                    rahaTilienNimet.RemoveAt(0);
                }
                else if (luoVahYksMenoTili && menoTilienNimet.Count > 0)
                {
                    luoVahYksMenoTili = false;

                    //Asetetaan tilinnimeksi ensimmäinen nimilistasta
                    tili.GetComponent<TilinScript>().tilinNimiEnum = menoTilienNimet[0];
                    tili.GetComponent<TilinScript>().tilinNimi = "Menotili";
                    tili.GetComponent<TilinScript>().tyyppi = tilinTyyppi.menotili;
                    tili.GetComponent<TilinScript>().AlustaTilinTiedot();
                    menoTilit.Add(tili);
                    //poistetaan käytetty tilinnimi listasta
                    menoTilienNimet.RemoveAt(0);
                }
                else if (luoVahYksTuloTili && tuloTilienNimet.Count > 0)
                {
                    luoVahYksTuloTili = false;

                    //Asetetaan nimeksi ensimmäinen nimilistastsa
                    tili.GetComponent<TilinScript>().tilinNimiEnum = tuloTilienNimet[0];
                    tili.GetComponent<TilinScript>().tilinNimi = "Tulotili";
                    tili.GetComponent<TilinScript>().tyyppi = tilinTyyppi.tulotili;
                    tili.GetComponent<TilinScript>().AlustaTilinTiedot();
                    tuloTilit.Add(tili);
                    //poistetaan käytetty tilinnimi listasta
                    tuloTilienNimet.RemoveAt(0);
                }
                else if (luoVahYksPaaomaTili && paaomaTilienNimet.Count > 0)
                {
                    luoVahYksPaaomaTili = false;

                    tili.GetComponent<TilinScript>().tilinNimiEnum = paaomaTilienNimet[0];
                    tili.GetComponent<TilinScript>().tilinNimi = "Pääomatili";
                    tili.GetComponent<TilinScript>().tyyppi = tilinTyyppi.paaomatili;
                    tili.GetComponent<TilinScript>().AlustaTilinTiedot();
                    paaomaTilit.Add(tili);
                    //poistetaan käytetty tilinnimi listasta
                    paaomaTilienNimet.RemoveAt(0);
                }//Seuraavaksi tarkistetaan onko tehtynä 2 eri tyypin tiliä
                else if (rahaTilit.Count < 1 && rahaTilienNimet.Count > 0)//Rahatiliä ei ole luotu mutta rahatilinnimiä on käytettävissä
                {
                    //OTetaan ensimmäinen nimi jonka pitäisi olla pankkitili
                    tili.GetComponent<TilinScript>().tilinNimiEnum = rahaTilienNimet[0];
                    tili.GetComponent<TilinScript>().tilinNimi = "Rahatili";
                    tili.GetComponent<TilinScript>().tyyppi = tilinTyyppi.rahatili;
                    tili.GetComponent<TilinScript>().AlustaTilinTiedot();
                    rahaTilit.Add(tili);
                    //poistetaan käytetty tilinnimi listasta
                    rahaTilienNimet.RemoveAt(0);
                }
                else if (menoTilit.Count < 1 && menoTilienNimet.Count > 0)//Menotilejä ei ole mutta menotilinimiä on vielä käytettävissä
                {

                    tili.GetComponent<TilinScript>().tilinNimiEnum = menoTilienNimet[0];
                    tili.GetComponent<TilinScript>().tilinNimi = "Menotili";
                    tili.GetComponent<TilinScript>().tyyppi = tilinTyyppi.menotili;
                    tili.GetComponent<TilinScript>().AlustaTilinTiedot();
                    menoTilit.Add(tili);
                    //poistetaan käytetty tilinnimi listasta
                    menoTilienNimet.RemoveAt(0);
                }
                else //Jos kaikki pakolliset tilit on tehty tehdään seuraavaksi tilejä randomisti
                {
                    bool naitaVoidaanTehda = false; //apumuuttuja jota käytetään kun tarkistellaan onko tilille vielä nimiä jäljellä
                    tilinTyyppi randomTilintyyppi = tilinTyyppi.rahatili; //randomTilintyyppi-muuttuja. OTetaan tähän muuttujaan whilen sisällä arvo avainDictionarystä. 
                    while (!naitaVoidaanTehda) //tehdään tätä niin pitkään kunnes osutaan randomilla sellaiseen tiliin jota voidaan vielä tehdä
                    {
                        Debug.Log("Naitavoidaan tehdä tarkistus sisällä");
                        int rnd = UnityEngine.Random.Range(0, randomTilienDictionaryAvaimet.Count); //Arvotaan randomi väliltä 0-kuinka monen tilin randomointi on enabloitu
                        int avainDictionaryyn = randomTilienDictionaryAvaimet[rnd]; //Otetaan listasta talteen randomnumeron kohdalta avain                    
                        randomTiliDictionary.TryGetValue(avainDictionaryyn, out randomTilintyyppi);
                        switch (randomTilintyyppi)
                        {
                            case tilinTyyppi.rahatili:
                                if (rahaTilienNimet.Count > 0)
                                    naitaVoidaanTehda = true;
                                break;
                            case tilinTyyppi.menotili:
                                if (menoTilienNimet.Count > 0)
                                    naitaVoidaanTehda = true;
                                break;
                            case tilinTyyppi.tulotili:
                                if (tuloTilienNimet.Count > 0)
                                    naitaVoidaanTehda = true;
                                break;
                            case tilinTyyppi.paaomatili:
                                if (paaomaTilienNimet.Count > 0)
                                    naitaVoidaanTehda = true;
                                break;
                            default:
                                Debug.Log("Täällä virhe!");
                                naitaVoidaanTehda = true;
                                break;
                        }
                    }
                    tili.GetComponent<TilinScript>().tyyppi = randomTilintyyppi;
                    switch (randomTilintyyppi)
                    {
                        case tilinTyyppi.menotili:
                            //randomoidaan tilin nimienum
                            int rndNimiNumero = UnityEngine.Random.Range(0, menoTilienNimet.Count);//otetaan siis jokin nimi rahatiliennimet-listasta
                            //Tarkistetaan onko tilin nimi jo käytetty apumuuttujalistasta

                            tili.GetComponent<TilinScript>().tilinNimiEnum = menoTilienNimet[rndNimiNumero];
                            tili.GetComponent<TilinScript>().tilinNimi = "Menotili nro." + (i + 1);
                            menoTilit.Add(tili);
                            //poistetaan käytetty tilinnimi listasta
                            menoTilienNimet.RemoveAt(rndNimiNumero);
                            break;
                        case tilinTyyppi.tulotili:
                            //randomoidaan tilin nimienum
                            int rndNimiNumero2 = UnityEngine.Random.Range(0, tuloTilienNimet.Count);//otetaan siis jokin nimi rahatiliennimet-listasta

                            tili.GetComponent<TilinScript>().tilinNimiEnum = tuloTilienNimet[rndNimiNumero2];
                            tili.GetComponent<TilinScript>().tilinNimi = "Tulotili nro." + (i + 1);
                            tuloTilit.Add(tili);
                            //poistetaan käytetty tilinnimi listasta
                            tuloTilienNimet.RemoveAt(rndNimiNumero2);
                            break;
                        case tilinTyyppi.rahatili:
                            //randomoidaan tilin nimienum
                            int rndNimiNumero3 = UnityEngine.Random.Range(0, rahaTilienNimet.Count);//otetaan siis jokin nimi rahatiliennimet-listasta

                            tili.GetComponent<TilinScript>().tilinNimiEnum = rahaTilienNimet[rndNimiNumero3];
                            tili.GetComponent<TilinScript>().tilinNimi = "Rahatili nro." + (i + 1);
                            rahaTilit.Add(tili);
                            //poistetaan käytetty tilinnimi listasta
                            rahaTilienNimet.RemoveAt(rndNimiNumero3);
                            break;
                        case tilinTyyppi.paaomatili:
                            //randomoidaan tilin nimienum
                            int rndNimiNumero4 = UnityEngine.Random.Range(0, paaomaTilienNimet.Count);//otetaan siis jokin nimi rahatiliennimet-listasta

                            tili.GetComponent<TilinScript>().tilinNimiEnum = paaomaTilienNimet[rndNimiNumero4];
                            tili.GetComponent<TilinScript>().tilinNimi = "Pääomatili nro." + (i + 1);
                            paaomaTilit.Add(tili);
                            //poistetaan käytetty tilinnimi listasta
                            paaomaTilienNimet.RemoveAt(rndNimiNumero4);
                            break;
                        default:
                            Debug.Log("Täällä virhe");
                            break;
                    }
                    tili.GetComponent<TilinScript>().AlustaTilinTiedot();
                }
                tili.GetComponent<TilinScript>().tilinId = i; //Asetetaan tilinsciptin id:ksi tämän loopin indeksinumero
                if (kuinkaTilitNaytetaan == tilienNayttoTapa.kiinteina)
                {
                    tili.transform.position = tilienPaikatCanvaksellaJosTilitKiinteina[i].transform.position; //asetetaan tilin fyysiseksi paikaksi taulukossa oleva paikka indeksinumerolla
                }
                else
                {
                    tili.transform.position = tilienSpawnObjectJosDropDown.transform.position;
                }
            }
        }

        //Seuraavassa osiossa generoidaan liiketapahtumat annettujen parametrien perusteella
        float kirjausRahaaJaljella = kirjauksienYhteisArvo;
        for (int i = 0; i < liiketapahtumiaKpl; i++)
        {
            LiikeTapahtuma tapahtuma = new LiikeTapahtuma(); //Tehdään uusi liiketapahtuma johon merkataan tapahtuman arvo sekä debit- ja kreditkirjaukset
            Kirjaus kirjaus1 = new Kirjaus(); //Ensimmäinen kirjaus
            Kirjaus kirjaus2 = new Kirjaus();//Toinen kirjaus
            float tamanKirjauksenArvo = 0.0f; //Alustetaan kirjaukselle arvo
            if (i < (liiketapahtumiaKpl - 1)) //Jos ei ole viimeinen liiketapahtuma niin randomoidaan arvo iffin sisällä
            {
                tamanKirjauksenArvo = UnityEngine.Random.Range(0.05f, 0.25f) * kirjausRahaaJaljella;
                //tamanKirjauksenArvo = Math.Round(tamanKirjauksenArvo,2); //arvo kahteen desimaaliin. Pitää käyttää doublea koska round
                string valiArvo = tamanKirjauksenArvo.ToString("F2");
                tamanKirjauksenArvo = float.Parse(valiArvo);
            }
            else { //Muuten arvona on jäljellä oleva rahamäärä
                tamanKirjauksenArvo = kirjausRahaaJaljella;
                string valiArvo = tamanKirjauksenArvo.ToString("F2");
                tamanKirjauksenArvo = float.Parse(valiArvo);
                //tamanKirjauksenArvo = Math.Round(tamanKirjauksenArvo,2);
            }
            kirjausRahaaJaljella -= tamanKirjauksenArvo; //Vähennetään käytettävästä rahasta tämän kirjauksen arvo
            kirjaus1.kirjauksenArvo = tamanKirjauksenArvo; //Laitetaan ensimmäisen kirjauksen muuttujan kirjauksenArvoksi tämänKirjauksenarvo
            kirjaus2.kirjauksenArvo = tamanKirjauksenArvo;

            //Alustetaan tuleeVaiMenee muuttuja ykköseksi, eli se tarkoittaa tällä hetkellä että yritykselle tulee jokin meno. Tämä siitä syystä että vielä ei tiedetä voiko tässä tehtävtyypissä olla tuloja
            int tuleeVaiMenee = 1;
            //Tarkistetaan onko tehtynä muita kuin meno ja rahatilejä. Jos ei ole muita niin tehtävässä ei voi merkitä kuin kuluja eli rahatililtä pois rahaa menotilille tai .
            if (tuloTilit.Count > 0 || paaomaTilit.Count > 0)
            { //jos on muita tilejä niin mennään tähän
                //Koska voi olla myös tuloja rahatilille niin randomoidaan onko nyt kyseessä tulo vai meno yrityksen tilille
                tuleeVaiMenee = UnityEngine.Random.Range(0, 2);
                if (tuleeVaiMenee == 0)//Nyt on kyseessä tulo eli jollekin rahatilille tehdään debit kirjaus
                {
                    //Ruvetaan täyttämään liiketapahtuman ensimmäisen kirjauksen tietoja. Kyseessä siis debit kirjaus jollekin pelissä olevista rahatileistä
                    int rndRahaTiliNumero = UnityEngine.Random.Range(0, rahaTilit.Count);
                    kirjaus1.kirjausTilinId = rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinId;
                    kirjaus1.tyyppi = kirjauksenTyyppi.debit; //debit kirjaus
                    //Rahaa voi tulla tilille joko tulotililtä, pääomatililtä tai toiselta tililtä. Tarkistetaan onko molempia tilejä olemassa vai vain toisia ja tehdään sen mukaisia toimia
                    if (tuloTilit.Count > 0 && paaomaTilit.Count > 0)
                    { //Jos on molempia mennään tähän
                        //Koska on molempia randomoidaan kummasta lähteestä raha tulee
                        //Koska on molempia tilityyppejä niin randomoidaan onko kyseessä tulo tulotililtä, siirto toiselta rahatililtä vai siirto rahatilille pääomatilitlä
                        int tuloRahaVaiPaa = UnityEngine.Random.Range(0, 3);
                        if (tuloRahaVaiPaa == 0)//Raha tulee rahatilille tulotililtä, tässä siis tulee kirjaus2 joka on nyt kredit kirjaus
                        {
                            int rndTuloTiliNumero = UnityEngine.Random.Range(0, tuloTilit.Count);
                            //jos tulo on korkotuloa tai osinkotuloja ja rahatilinä on kassatili niin arvotaan rahatili uudestaan koska käteisenä näitä ei voi ottaa
                            if ((tuloTilit[rndTuloTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.korkotulot || tuloTilit[rndTuloTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.osinkotulot) && rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.kassatili) {
                                while (rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.kassatili) { //arvotaan tiliä niin pitkään kunnes se ei ole kassatili
                                    rndRahaTiliNumero = UnityEngine.Random.Range(0,rahaTilit.Count);
                                }
                            }
                            //päivitetään kirjaus1 tietoja
                            kirjaus1.kirjausTilinId = rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinId;
                            //kirjaus2 tiedot
                            kirjaus2.tyyppi = kirjauksenTyyppi.kredit;
                            kirjaus2.kirjausTilinId = tuloTilit[rndTuloTiliNumero].GetComponent<TilinScript>().tilinId;
                            kirjaukset.Add(kirjaus1);
                            kirjaukset.Add(kirjaus2);
                            tapahtuma.debitKirjaus = kirjaus1;
                            tapahtuma.kreditKirjaus = kirjaus2;
                            tapahtuma.tapahtumaKuvaus = GeneroiTapahtumanKuvaus(rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum, tuloTilit[rndTuloTiliNumero].GetComponent<TilinScript>().tilinNimiEnum, tamanKirjauksenArvo);
                            //tapahtuma.tapahtumaKuvaus = "Yrityksen tilille tulee esim.  vuokratuloja. Merkitse " + tamanKirjauksenArvo + " euroa tilille " + rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum.ToString() + " Tee myös kredit kirjaus tilille " + tuloTilit[rndTuloTiliNumero].GetComponent<TilinScript>().tilinNimiEnum.ToString();
                            liikeTapahtumat.Add(tapahtuma);
                        }
                            else if( tuloRahaVaiPaa == 1 && rahaTilit.Count > 1) //Huom tässä tarkistetaan myös että rahatilejä on käytössä enemmän kuin 2. Jos rahatilejä olisi vain yksi niin siirrettäisiin rahaa samalle tilille joka ei ole mahdollista!
                        { //Raha tulee tilille toiselta rahatililtä. Tässä siis tulee kirjaus 2 joka on nyt kredit kirjaus
                            int rndRahaTiliNumero2 = UnityEngine.Random.Range(0, rahaTilit.Count);
                            while (rahaTilit[rndRahaTiliNumero2].GetComponent<TilinScript>().tilinId == rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinId) { //jos debit kirjauksen tilin id on sama kuin kredit kirjauksen niin arvotaan tili uudestaan
                                rndRahaTiliNumero2 = UnityEngine.Random.Range(0,rahaTilit.Count);
                            }
                            kirjaus2.tyyppi = kirjauksenTyyppi.kredit;
                            kirjaus2.kirjausTilinId = rahaTilit[rndRahaTiliNumero2].GetComponent<TilinScript>().tilinId;
                            kirjaukset.Add(kirjaus1);
                            kirjaukset.Add(kirjaus2);
                            tapahtuma.debitKirjaus = kirjaus1;
                            tapahtuma.kreditKirjaus = kirjaus2;
                            tapahtuma.tapahtumaKuvaus = GeneroiTapahtumanKuvaus(rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum, rahaTilit[rndRahaTiliNumero2].GetComponent<TilinScript>().tilinNimiEnum, tamanKirjauksenArvo);   
                            //tapahtuma.tapahtumaKuvaus = "Siirrät varoja tilien välillä. Merkitse " + tamanKirjauksenArvo + " euroa tulona tilille " + rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum.ToString() + " Tee myös kredit kirjaus tilille " + rahaTilit[rndRahaTiliNumero2].GetComponent<TilinScript>().tilinNimiEnum.ToString();
                            liikeTapahtumat.Add(tapahtuma);
                        }
                        else
                        { //Raha tulee tilille pääomatililtä. Tässä siis tulee kirjaus 2 joka on nyt kredit kirjaus
                            int rndPaaomaTiliNumero = UnityEngine.Random.Range(0, paaomaTilit.Count);
                                //Raha voi tulla tilille Lainana,  mutta ei yksityiskäyttöraha(osinkojen jakona tai tavallaan yrittäjän palkka) tai ostovelkana. Ostovelkaa voidaan maksaa pois rahatililtä. Ostovelka kasvaa kreditissä kun tulee meno(debit) menotilille. Eli jos arvottu tili on yksityiskäyttötili niin arvotaan se uudestaan
                                //Koska alussa luodaan aina vähintään 1 lainatili niin ei tarvitse tarkistaa onko tällaista tiliä olemassa
                            while (paaomaTilit[rndPaaomaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum != tilinNimi.lainat)
                            {
                                rndPaaomaTiliNumero = UnityEngine.Random.Range(0,paaomaTilit.Count);
                            }
                            //Lainaa ei voi ottaa kassaan. Jos rahatilinä on kassatili niin arvotaan se uudestaan
                            while(rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.kassatili){
                                rndRahaTiliNumero = UnityEngine.Random.Range(0,rahaTilit.Count);
                            }
                            //päivitettän kirjaus 1 tietoja
                            kirjaus1.kirjausTilinId = rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinId;
                            //kirjaus2
                            kirjaus2.tyyppi = kirjauksenTyyppi.kredit;
                            kirjaus2.kirjausTilinId = paaomaTilit[rndPaaomaTiliNumero].GetComponent<TilinScript>().tilinId;
                            kirjaukset.Add(kirjaus1);
                            kirjaukset.Add(kirjaus2);
                            tapahtuma.debitKirjaus = kirjaus1;
                            tapahtuma.kreditKirjaus = kirjaus2;
                            tapahtuma.tapahtumaKuvaus = GeneroiTapahtumanKuvaus(rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum, paaomaTilit[rndPaaomaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum, tamanKirjauksenArvo);
                            //tapahtuma.tapahtumaKuvaus = "Yrityksen tilille tulee rahaa vaikkapa lainana pääomatililtä. Merkitse " + tamanKirjauksenArvo + " euroa tulona tilille " + rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum.ToString() + " Tee myös kredit kirjaus tilille " + paaomaTilit[rndPaaomaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum.ToString();
                            liikeTapahtumat.Add(tapahtuma);
                        }
                    }
                    else if (tuloTilit.Count > 0)
                    { //jos ei ollut molemman tyyppisiä tilejä ja tulotilejä on enemmän kuin 0 niin mennään tähän. Yritykselle tulee siis tuloja jonkin tulonlähteen kautta
                        int rndTuloTiliNumero = UnityEngine.Random.Range(0, tuloTilit.Count);
                        //jos tulonlähde on osinkotulo tai korkotulo niin tarkistetaan onko tili jolle ollaan laittamassa kassatili. Jos on kassatili niin arvotaan rahatili uudestaan
                        if ((tuloTilit[rndTuloTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.osinkotulot || tuloTilit[rndTuloTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.korkotulot) && rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.kassatili) {
                            while (rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.kassatili) { //arvotaan niin pitkään kunnnes ei ole kassatili
                                rndRahaTiliNumero = UnityEngine.Random.Range(0,rahaTilit.Count);
                            }
                            //täytetään sitten rahatilin tietoja uusiksi niin sinne ei jää vanhaa tietoa
                            kirjaus1.kirjausTilinId = rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinId;
                        }
                        kirjaus2.tyyppi = kirjauksenTyyppi.kredit;
                        kirjaus2.kirjausTilinId = tuloTilit[rndTuloTiliNumero].GetComponent<TilinScript>().tilinId;
                        kirjaukset.Add(kirjaus1);
                        kirjaukset.Add(kirjaus2);
                        tapahtuma.debitKirjaus = kirjaus1;
                        tapahtuma.kreditKirjaus = kirjaus2;
                        tapahtuma.tapahtumaKuvaus = GeneroiTapahtumanKuvaus(rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum, tuloTilit[rndTuloTiliNumero].GetComponent<TilinScript>().tilinNimiEnum, tamanKirjauksenArvo);
                        //tapahtuma.tapahtumaKuvaus = "Yrityksen tilille tulee tuloja. Merkitse " + tamanKirjauksenArvo + " euroa tulona tilille " + rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum.ToString() + " Tee myös kredit kirjaus tilille " + tuloTilit[rndTuloTiliNumero].GetComponent<TilinScript>().tilinNimiEnum.ToString();
                        liikeTapahtumat.Add(tapahtuma);
                    }
                    else if (paaomaTilit.Count > 0)
                    { //jos on pääomatilejä mennään tähän. Yrityksen tilille tulee siis rahaa jonkin pääoman kautta. Esim laina
                        int rndPaaomaTiliNumero = UnityEngine.Random.Range(0, paaomaTilit.Count);
                        //tulo tilille ei voi tulla yksityiskäyttöpääomana tai ostovelkana tässä kohtaa, jos siis pääomatili on tällainen niin arvotaan se uudestaan
                        while(paaomaTilit[rndPaaomaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.yksityiskäyttö || paaomaTilit[rndPaaomaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.ostovelat){
                            rndPaaomaTiliNumero = UnityEngine.Random.Range(0,paaomaTilit.Count);
                        }
                        //Pääomaa ei voi myöskään sijoittaa kassaa. Jos rahatilinä on kassatili niin arvotaan se uudestaan
                        while (rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.kassatili)
                        {
                            rndRahaTiliNumero = UnityEngine.Random.Range(0, rahaTilit.Count);
                        }
                        //päivitettän kirjaus 1 tietoja
                        kirjaus1.kirjausTilinId = rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinId;
                        //kirjaus2
                        kirjaus2.tyyppi = kirjauksenTyyppi.kredit;
                        kirjaus2.kirjausTilinId = paaomaTilit[rndPaaomaTiliNumero].GetComponent<TilinScript>().tilinId;
                        kirjaukset.Add(kirjaus1);
                        kirjaukset.Add(kirjaus2);
                        tapahtuma.debitKirjaus = kirjaus1;
                        tapahtuma.kreditKirjaus = kirjaus2;
                        tapahtuma.tapahtumaKuvaus = GeneroiTapahtumanKuvaus(rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum, paaomaTilit[rndPaaomaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum, tamanKirjauksenArvo);
                        //tapahtuma.tapahtumaKuvaus = "Yrityksen tilille tulee tuloja lainana. Merkitse " + tamanKirjauksenArvo + " euroa tulona tilille " + rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum.ToString() + " Tee myös kredit kirjaus tilille " + paaomaTilit[rndPaaomaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum.ToString();
                        liikeTapahtumat.Add(tapahtuma);
                    }
                }
                else
                { //Nyt on kyseessä meno eli yritykseltä lähtee rahaa jollekin menotilille tai pääomatilille. Menon voi maksaa joko rahatililtä(ei talletus) tai ostovelalla jolloin se tulee pääomatililtä. Tarkistetaan siis ensin onko olemassa pääomatilejä ja edetään sen mukaan                 
                    if (paaomaTilit.Count > 0)
                    { //jos on pääomatilejä
                        //Arvotaan ensin siirretäänkö rahaa pääomatilille eli esim yksityiseen käyttöön rahatililtä vai onko kyseessä jokin menotilitapahtuma
                        int rndSiirtopaaomaanVaiMeno = UnityEngine.Random.Range(0,2);
                        if (rndSiirtopaaomaanVaiMeno == 0)
                        { //siirto rahatililtä pääomatilille
                            //randomoidaan ensin pääomatili. Jos sattuu kohdalle yksityiskäyttötili niin sille voidaan siirtää rahaa myös talletustililtä
                            //Randomoidaan pääomatili jolle raha merkitään ja täytetään kirjaus2 tiedot
                            int rndPaaomaTiliNumero = UnityEngine.Random.Range(0, paaomaTilit.Count);
                            //rahaa voi siirtää rahatililtä yksityiskäyttötilille, maksaa ostovelkaa pois tai maksaa lainaa pois. Eli jos sattui kohdalle pääomatili niin arvotaan tili uudestaan. Olemassa on aina 1 lainatili joten ei tarvitse tarkistaa onko tilejä muitakin kuin pääoma
                            while (paaomaTilit[rndPaaomaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.omapääoma)
                            {
                                rndPaaomaTiliNumero = UnityEngine.Random.Range(0, paaomaTilit.Count);
                            }
                            kirjaus2.tyyppi = kirjauksenTyyppi.debit; //debit kirjaus pääomatilille
                            kirjaus2.kirjausTilinId = paaomaTilit[rndPaaomaTiliNumero].GetComponent<TilinScript>().tilinId;



                            //Täytetään kirjauksen1 tiedot. Tämä kirjaus lähtee joltain rahatililtä ja se on siis kredit kirjaus
                            int rndRahaTiliNumero = UnityEngine.Random.Range(0, rahaTilit.Count);
                            //Ei voida maksaa laskuja talletustililtä, eli jos arvonnassa sattui kohdalle ostovelka niin arvotaan tili uudestaan
                            if (paaomaTilit[rndPaaomaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.ostovelat && rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.pankkitilitalletus)
                            {
                                //eli jos pääomatiliksi sattui ostovelka tili ja rahatilinä talletustili niin arvotaan rahatiliä uudestaan. jos pääomatilinä oli yksityisnosto tai laina niin se voidaan tehdä myös talletustililtä, eli tänne ei tulla arpomaan tiliä uudestaan
                                while (rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.pankkitilitalletus)
                                {
                                    rndRahaTiliNumero = UnityEngine.Random.Range(0, rahaTilit.Count);
                                }
                            }
                            kirjaus1.tyyppi = kirjauksenTyyppi.kredit;//kredit koska rahatililtä lähtee rahaa
                            kirjaus1.kirjausTilinId = rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinId;
                            
                            kirjaukset.Add(kirjaus1);
                            kirjaukset.Add(kirjaus2);
                            tapahtuma.debitKirjaus = kirjaus2;
                            tapahtuma.kreditKirjaus = kirjaus1;
                            tapahtuma.tapahtumaKuvaus = GeneroiTapahtumanKuvaus(paaomaTilit[rndPaaomaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum, rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum, tamanKirjauksenArvo);
                            //tapahtuma.tapahtumaKuvaus = "Yritys siirtää rahaa esim yksityiskäyttöön pääomatilille rahatililtä. Merkitse " + tamanKirjauksenArvo + " euroa menona tilille " + rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum.ToString() + " Tee myös debit kirjaus tilille " + paaomaTilit[rndPaaomaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum.ToString();
                            liikeTapahtumat.Add(tapahtuma);
                        }
                        else //Kyseessä meno joka maksetaan rahatililtä tai pääomalla
                        {
                            //Täytetään ensimmäisen kirjauksen tieto. tiedetään että kyseessä on kulu menotilille, eli jollekin menotilille tehdään debit kirjaus
                            int rndMenoTiliNumero = UnityEngine.Random.Range(0, menoTilit.Count);
                            kirjaus1.tyyppi = kirjauksenTyyppi.debit;//Debit koska meno kasvaa
                            kirjaus1.kirjausTilinId = menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinId;
                            //Pääomatileistä vain ostovelkaa voidaan käyttää menojen kattamiseen, tarkistetaan siis onko pääomatilien joukossa ostovelka-tiliä ja edetään sen mukaan
                            bool loytyyOstovelka = false;
                            for (int j = 0; j < paaomaTilit.Count; j++)
                            {
                                if (paaomaTilit[j].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.ostovelat) {
                                    Debug.Log("Löytyi ostovelkatili");
                                    loytyyOstovelka = true;
                                }
                            }
                            //Jos ostovelkatili löytyi niin randomoidaan mistä meno katetaan, jos ei löydy niin alustetaan muuttuja vain sopivasti menemään oikeaan kohtaan
                            int rndRahaVaiPaaoma = 0;
                            if (loytyyOstovelka) {
                                rndRahaVaiPaaoma = UnityEngine.Random.Range(0, 2);
                            }
                            
                            if (rndRahaVaiPaaoma == 0)
                            { //rahatililtä
                                //Randomoidaan rahatili ja täytetään kirjaus2 tiedot
                                int rndRahaTiliNumero = UnityEngine.Random.Range(0, rahaTilit.Count);
                                //arvotaa tiliä niin pitkään uudestaan kunnes se ei ole talletustili. Talletustililtä ei voida maksaa mitään koska se on vain talletuksia varten! Rahatileissä on aina pakollisesti myös pankkitili, joten ei tarvitse tarkistaa löytyykö muita kuin talletustilejä
                                while (rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.pankkitilitalletus) {
                                    rndRahaTiliNumero = UnityEngine.Random.Range(0,rahaTilit.Count);
                                }
                                //jos meno on korkomeno tai vero ja tili jolta ollaan maksamassa kassatili niin arvotaan rahatili uudestaan. Käteisellä tai talletustililtä ei maksella korkomenoja tai veroja
                                if ((menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.korkomenot || menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.verot) && rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.kassatili) {
                                    while (rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.kassatili || rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.pankkitilitalletus) { //arvotaan niin pitkään uudestaan kunnes tili on haluama
                                        rndRahaTiliNumero = UnityEngine.Random.Range(0, rahaTilit.Count);
                                    }
                                }
                                kirjaus2.tyyppi = kirjauksenTyyppi.kredit; //kredit kirjaus koska rahatililtä lähtee rahaa
                                kirjaus2.kirjausTilinId = rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinId;
                                kirjaukset.Add(kirjaus1);
                                kirjaukset.Add(kirjaus2);
                                tapahtuma.debitKirjaus = kirjaus1;
                                tapahtuma.kreditKirjaus = kirjaus2;
                                tapahtuma.tapahtumaKuvaus = GeneroiTapahtumanKuvaus(menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum, rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum, tamanKirjauksenArvo);
                                //tapahtuma.tapahtumaKuvaus = "Yritykselle tulee menoja. Merkitse " + tamanKirjauksenArvo + " euroa menona tilille " + menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum.ToString() + " Tee myös kredit kirjaus tilille " + rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum.ToString();
                                liikeTapahtumat.Add(tapahtuma);
                            }
                            else
                            { //pääomatililtä
                                
                                //Randomoidaan pääomatili ja täytetään kirjaus2 tiedot
                                int rndPaaomaTiliNumero = UnityEngine.Random.Range(0, paaomaTilit.Count);
                                //Menoa ei voi maksaa pääomatililtä, lainarahalla tai yksityiskäyttöpääomatililtä. Randomoidaan siis tili uudestaan mikäli tällainen tili sattui kohdalle. Alussa luodaan aina pakollisesti yksi Lainatili joten ei tarvitse tarkistaa onko sellaista olemassa
                                while (paaomaTilit[rndPaaomaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.omapääoma || paaomaTilit[rndPaaomaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.yksityiskäyttö || paaomaTilit[rndPaaomaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.lainat) {
                                    rndPaaomaTiliNumero = UnityEngine.Random.Range(0,paaomaTilit.Count);
                                }
                                //jos pääomatiliksi sattuu ostovelka niin tulee rajoituksia. Ostovelalla voi maksaa vain tavaran, sähkömenot, mainsomenot, vuokran, siivousmenot, toimistotarvikkeet ja ajoneuvokustannukset
                                //jos pääomatili siis ostovelka niin tarkistetaan onko menotili jokin yllä mainituista, jos ei ole niin arvotaan niin pitkään kunnes on
                                if (paaomaTilit[rndPaaomaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.ostovelat && (menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum != tilinNimi.ostot && menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum != tilinNimi.sähkömenot && menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum != tilinNimi.mainosmenot && menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum != tilinNimi.vuokramenot && menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum != tilinNimi.siivoustarvikkeet && menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum != tilinNimi.toimistotarvikkeet && menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum != tilinNimi.ajoneuvomenot))
                                {
                                    while (menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum != tilinNimi.ostot && menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum != tilinNimi.sähkömenot && menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum != tilinNimi.mainosmenot && menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum != tilinNimi.vuokramenot && menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum != tilinNimi.siivoustarvikkeet && menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum != tilinNimi.toimistotarvikkeet && menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum != tilinNimi.ajoneuvomenot)
                                    {
                                        rndMenoTiliNumero = UnityEngine.Random.Range(0,menoTilit.Count);
                                    }
                                    //päivitetään kirjaus 1 eli menotilin tietoja
                                    kirjaus1.kirjausTilinId = menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinId;
                                }



                                kirjaus2.tyyppi = kirjauksenTyyppi.kredit; //kredit kirjaus koska rahatililtä lähtee rahaa
                                kirjaus2.kirjausTilinId = paaomaTilit[rndPaaomaTiliNumero].GetComponent<TilinScript>().tilinId;
                                kirjaukset.Add(kirjaus1);
                                kirjaukset.Add(kirjaus2);
                                tapahtuma.debitKirjaus = kirjaus1;
                                tapahtuma.kreditKirjaus = kirjaus2;
                                tapahtuma.tapahtumaKuvaus = GeneroiTapahtumanKuvaus(menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum, paaomaTilit[rndPaaomaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum, tamanKirjauksenArvo);
                                //tapahtuma.tapahtumaKuvaus = "Yritykselle tulee menoja jonka maksamiseen käytetään laskua. Merkitse " + tamanKirjauksenArvo + " euroa menona tilille " + menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum.ToString() + " Tee myös kredit kirjaus tilille " + paaomaTilit[rndPaaomaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum.ToString();
                                liikeTapahtumat.Add(tapahtuma);
                            }
                        }
                    }
                    else
                    { //Ei ollut pääomatilejä eli katetaan kulu rahatililtä. Kopioidaan rahatilin käyttökoodi yläpuolelta.
                        //Täytetään ensimmäisen kirjauksen tieto. tiedetään että kyseessä on kulu menotilille, eli jollekin menotilille tehdään debit kirjaus
                        int rndMenoTiliNumero = UnityEngine.Random.Range(0, menoTilit.Count);
                        kirjaus1.tyyppi = kirjauksenTyyppi.debit;//Debit koska meno kasvaa
                        kirjaus1.kirjausTilinId = menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinId;
                        //Randomoidaan rahatili ja täytetään kirjaus2 tiedot
                        int rndRahaTiliNumero = UnityEngine.Random.Range(0, rahaTilit.Count);
                        //menoa ei voida maksaa talletustilitlä. Jos sattuu talletustili niin arvotaan tili uudestaan. Käytössä on aina vähintään 1 pankkitili niin ei tarvitse tarkistaa löytyykö sitä
                        while (rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.pankkitilitalletus) {
                            rndRahaTiliNumero = UnityEngine.Random.Range(0,rahaTilit.Count);
                        }
                        //jos meno on korkomeno tai vero ja tili jolta ollaan maksamassa kassatili niin arvotaan rahatili uudestaan. Käteisellä tai talletustililtä ei maksella korkomenoja tai veroja
                        if ((menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.korkomenot || menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.verot) && rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.kassatili)
                        {
                            while (rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.kassatili || rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.pankkitilitalletus)
                            { //arvotaan niin pitkään uudestaan kunnes tili on haluama
                                rndRahaTiliNumero = UnityEngine.Random.Range(0, rahaTilit.Count);
                            }
                        }
                        kirjaus2.tyyppi = kirjauksenTyyppi.kredit; //kredit kirjaus koska rahatililtä lähtee rahaa
                        kirjaus2.kirjausTilinId = rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinId;
                        kirjaukset.Add(kirjaus1);
                        kirjaukset.Add(kirjaus2);
                        tapahtuma.debitKirjaus = kirjaus1;
                        tapahtuma.kreditKirjaus = kirjaus2;
                        tapahtuma.tapahtumaKuvaus = GeneroiTapahtumanKuvaus(menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum, rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum, tamanKirjauksenArvo);
                        //tapahtuma.tapahtumaKuvaus = "Yritykselle tulee menoja. Merkitse " + tamanKirjauksenArvo + " euroa menona tilille " + menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum.ToString() + " Tee myös kredit kirjaus tilille " + rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum.ToString();
                        liikeTapahtumat.Add(tapahtuma);
                    }

                }
            }
            else { //ei ollut tulo tai pääomatilejä eli tulee vain kuluja eli rahatililtä pois rahaa menotilille tai jos rahatilejä enemmän kuin 1 niin voidaan tehdä siirtoja tilien välillä.
                //Täytetään kirjaus1 tiedot, eli rahaa pois rahatililtä, eli kredit kirjaus
                int rndRahaTiliNumero = UnityEngine.Random.Range(0,rahaTilit.Count);
                kirjaus1.kirjausTilinId = rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinId;
                kirjaus1.tyyppi = kirjauksenTyyppi.kredit; //Kredit eli tililtä lähtee rahaa
                //Katsotaan onko rahatilejä enemmän kuin 1, jos on enemmän niin randomoidaan siirretäänkö rahaa tilien välillä vai onko kyseessä jokin meno
                if (rahaTilit.Count > 1)
                {
                    int rndSiirtoVaiMeno = UnityEngine.Random.Range(0, 2);
                    if (rndSiirtoVaiMeno == 0)
                    { //Nyt kyseessä siirto eli tehdään debit kirjaus jollekin rahatilille
                        int rndRahaTiliNumero2 = UnityEngine.Random.Range(0, rahaTilit.Count);
                        while (rndRahaTiliNumero == rndRahaTiliNumero2) { //jos äsken arvottu tili on sama kuin kirjaus1:sen tili niin arvotaan tili uudestaan
                            rndRahaTiliNumero2 = UnityEngine.Random.Range(0,rahaTilit.Count);
                        }
                        kirjaus2.kirjausTilinId = rahaTilit[rndRahaTiliNumero2].GetComponent<TilinScript>().tilinId;
                        kirjaus2.tyyppi = kirjauksenTyyppi.debit; // Debit eli tälle rahatilille siirretään rahaa
                        kirjaukset.Add(kirjaus1);
                        kirjaukset.Add(kirjaus2);
                        tapahtuma.debitKirjaus = kirjaus2;
                        tapahtuma.kreditKirjaus = kirjaus1;
                        tapahtuma.tapahtumaKuvaus = GeneroiTapahtumanKuvaus(rahaTilit[rndRahaTiliNumero2].GetComponent<TilinScript>().tilinNimiEnum, rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum, tamanKirjauksenArvo);
                        //tapahtuma.tapahtumaKuvaus = "Siirrät rahaa tililtä toiselle. Merkitse " + tamanKirjauksenArvo + " euroa menona tilille " + rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum.ToString() + " Tee myös debit kirjaus tilille " + rahaTilit[rndRahaTiliNumero2].GetComponent<TilinScript>().tilinNimiEnum.ToString();
                        liikeTapahtumat.Add(tapahtuma);
                    }
                    else
                    { //nyt kyseessä meno eli tehdään debit kirjaus jollekin menotilille
                        //koska kyseessä meno niin sitä ei voi maksaa talletustililtä. Jos aiemmin arvottu rahatili on talletustili niin arvotaan se uudestaan
                        while (rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.pankkitilitalletus) {
                            rndRahaTiliNumero = UnityEngine.Random.Range(0,rahaTilit.Count);
                        }
                        
                        
                        //kirjaus2 tiedot
                        int rndMenoTiliNumero = UnityEngine.Random.Range(0, menoTilit.Count);
                        //jos meno on korkomeno tai vero ja tili jolta ollaan maksamassa kassatili niin arvotaan rahatili uudestaan. Käteisellä tai talletustililtä ei maksella korkomenoja tai veroja
                        if ((menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.korkomenot || menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.verot) && rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.kassatili)
                        {
                            while (rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.kassatili || rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.pankkitilitalletus)
                            { //arvotaan niin pitkään uudestaan kunnes tili on haluama
                                rndRahaTiliNumero = UnityEngine.Random.Range(0, rahaTilit.Count);
                            }
                        }
                        //ja täytetään kirjaus1:n tietoja uusikis
                        kirjaus1.kirjausTilinId = rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinId;

                        kirjaus2.kirjausTilinId = menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinId;
                        kirjaus2.tyyppi = kirjauksenTyyppi.debit; // Debit eli menot kasvavat
                        kirjaukset.Add(kirjaus1);
                        kirjaukset.Add(kirjaus2);
                        tapahtuma.debitKirjaus = kirjaus2;
                        tapahtuma.kreditKirjaus = kirjaus1;
                        tapahtuma.tapahtumaKuvaus = GeneroiTapahtumanKuvaus(menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum, rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum, tamanKirjauksenArvo);
                        //tapahtuma.tapahtumaKuvaus = "Yritykselle tulee menoja. Merkitse " + tamanKirjauksenArvo + " euroa menona tilille " + menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum.ToString() + " Tee myös kredit kirjaus tilille " + rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum.ToString();
                        liikeTapahtumat.Add(tapahtuma);
                    }
                }
                else { //Jos rahatilejä vain 1 niin silloin pakko olla kyseessä meno
                    //koska kyseessä meno niin sitä ei voi maksaa talletustililtä. Jos aiemmin arvottu rahatili on talletustili niin arvotaan se uudestaan
                    while (rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.pankkitilitalletus)
                    {
                        rndRahaTiliNumero = UnityEngine.Random.Range(0, rahaTilit.Count);
                    }
                   
                    

                    //kirjaus2
                    int rndMenoTiliNumero = UnityEngine.Random.Range(0, menoTilit.Count);
                    //jos meno on korkomeno tai vero ja tili jolta ollaan maksamassa kassatili niin arvotaan rahatili uudestaan. Käteisellä tai talletustililtä ei maksella korkomenoja tai veroja
                    if ((menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.korkomenot || menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.verot) && rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.kassatili)
                    {
                        while (rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.kassatili || rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum == tilinNimi.pankkitilitalletus)
                        { //arvotaan niin pitkään uudestaan kunnes tili on haluama
                            rndRahaTiliNumero = UnityEngine.Random.Range(0, rahaTilit.Count);
                        }
                    }
                    //ja täytetään kirjaus1:n tietoja uusikis
                    kirjaus1.kirjausTilinId = rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinId;

                    kirjaus2.kirjausTilinId = menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinId;
                    kirjaus2.tyyppi = kirjauksenTyyppi.debit; // Debit eli menot kasvavat
                    kirjaukset.Add(kirjaus1);
                    kirjaukset.Add(kirjaus2);
                    tapahtuma.debitKirjaus = kirjaus2;
                    tapahtuma.kreditKirjaus = kirjaus1;
                    tapahtuma.tapahtumaKuvaus = GeneroiTapahtumanKuvaus(menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum, rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum, tamanKirjauksenArvo);
                    //tapahtuma.tapahtumaKuvaus = "Yritykselle tulee menoja. Merkitse " + tamanKirjauksenArvo + " euroa menona tilille " + menoTilit[rndMenoTiliNumero].GetComponent<TilinScript>().tilinNimiEnum.ToString() + " Tee myös kredit kirjaus tilille " + rahaTilit[rndRahaTiliNumero].GetComponent<TilinScript>().tilinNimiEnum.ToString();
                    liikeTapahtumat.Add(tapahtuma);
                }
            }

            

            
        }
        Debug.Log("Kirjaukset tehty");
    }


    private string GeneroiTapahtumanKuvaus(tilinNimi debitKirjausTilinNimi, tilinNimi kreditKirjausTilinNimi, float summa)
    { //Metodi jolla generoidaan tapahtumankuvaus realistiseksi
        //debit kirjauksia voi olla tileillä meno, raha ja pääoma. Kredit kirjauksia tileillä tulo, raha ja pääoma
        //tehdään ensin switch jossa käydään läpi kaikki mahdolliset debitkirjauksien vaihtoehdot eli kaikki mahdolliset meno, raha ja pääomatilien nimet(aika monta siis)
        if (vaikeustaso == tehtavienKuvausVaikeusTaso.helppo)
        {
            switch (debitKirjausTilinNimi)
            {
                //tästä eteenpäin kaikki menotilejä
                case tilinNimi.ostot:
                    //Ja casen sisään toinen switch jossa käsitellään sitten kreditkirjauksentilit. Kreditkirjauksia voi olla raha, pääoma ja tulotileillä. Ostoja ei voi kuitenkaan suoraan kattaa tulotileillä eli menotileille käydään läpi vain raha ja pääomatilit
                    switch (kreditKirjausTilinNimi)
                    {
                        case tilinNimi.kassatili:
                            //sisempien casejen sisällä tehdään nyt sitten lopullinen palautettava tapahtumakuvaus. Nyt on siis tiedossa minkä nimiseltä tililtä lähtee minkälainen kirjaus ja mille tilille
                            Debug.Log("Tapahtuma nro. 1");
                            return "Yritys ostaa tavaraa käteisellä. Rahamäärä: " +  summa.ToString().Replace(".",",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.ostot.ToString() + " ja kredit kirjaus tilille " + tilinNimi.kassatili.ToString();
                            break;
                        case tilinNimi.pankkitili:
                            Debug.Log("Tapahtuma nro. 2");
                            return "Yritys ostaa tavaraa käyttäen pankkitiliä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.ostot.ToString() + " ja kredit kirjaus tilille " + tilinNimi.pankkitili.ToString();
                            break;
                        case tilinNimi.pankkitilikaytto:
                            Debug.Log("Tapahtuma nro. 3");
                            return "Yritys ostaa tavaraa käyttäen käyttöpankkitiliä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.ostot.ToString() + " ja kredit kirjaus tilille " + tilinNimi.pankkitilikaytto.ToString();
                            break;
                        case tilinNimi.ostovelat:
                            Debug.Log("Tapahtuma nro. 4");
                            return "Yritys ostaa tavaraa laskulla, joka merkitään ostovelaksi. Laskun summa: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.ostot.ToString() + " ja kredit kirjaus tilille " + tilinNimi.ostovelat.ToString();
                            break;
                    }
                    break;
                case tilinNimi.palkat:
                    switch (kreditKirjausTilinNimi)
                    {
                        case tilinNimi.kassatili:
                            Debug.Log("Tapahtuma nro. 5");
                            return "Yritys maksaa työntekijöiden palkkoja käteisellä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.palkat.ToString() + " ja kredit kirjaus tilille " + tilinNimi.kassatili.ToString();
                            break;
                        case tilinNimi.pankkitili:
                            Debug.Log("Tapahtuma nro. 6");
                            return "Yritys maksaa työntekijöiden palkkoja pankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.palkat.ToString() + " ja kredit kirjaus tilille " + tilinNimi.pankkitili.ToString();
                            break;
                        case tilinNimi.pankkitilikaytto:
                            Debug.Log("Tapahtuma nro. 7");
                            return "Yritys maksaa työntekijöiden palkkoja käyttöpankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.palkat.ToString() + " ja kredit kirjaus tilille " + tilinNimi.pankkitilikaytto.ToString();
                            break;
                    }
                    break;
                case tilinNimi.sähkömenot:
                    //return "Yritys maksaa sähkölaskun tililtä " + kreditKirjausTilinNimi.ToString() + " Tee debit kirjaus tilille " + tilinNimi.sähkömenot.ToString() + " ja kreditkirjaus tilille " + kreditKirjausTilinNimi.ToString();
                    switch (kreditKirjausTilinNimi)
                    {
                        case tilinNimi.kassatili:
                            Debug.Log("Tapahtuma nro. 8");
                            return "Yritys maksaa sähkölaskun käteisellä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.sähkömenot.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitili:
                            Debug.Log("Tapahtuma nro. 9");
                            return "Yritys maksaa sähkölaskun tilisiirtona pankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.sähkömenot.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitilikaytto:
                            Debug.Log("Tapahtuma nro. 10");
                            return "Yritys maksaa sähkölaskun tilisiirtona käyttöpankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.sähkömenot.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.ostovelat:
                            Debug.Log("Tapahtuma nro. 11");
                            return "Yritys saa sähkölaskun, joka kirjataan ostovelaksi. Laskun summa: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.sähkömenot.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                    }
                    break;
                case tilinNimi.mainosmenot:
                    //return "Yritys maksaa mainosmenoja tililtä " + kreditKirjausTilinNimi.ToString() + " Tee debit kirjaus tilille " + tilinNimi.mainosmenot.ToString() + " ja kreditkirjaus tilille " + kreditKirjausTilinNimi.ToString();
                    switch (kreditKirjausTilinNimi)
                    {
                        case tilinNimi.kassatili:
                            Debug.Log("Tapahtuma nro. 12");
                            return "Yritys maksaa mainosmenoja käteisellä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.mainosmenot.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitili:
                            Debug.Log("Tapahtuma nro. 13");
                            return "Yritys maksaa mainosmenoja tilisiirtona pankkitililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.mainosmenot.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitilikaytto:
                            Debug.Log("Tapahtuma nro. 14");
                            return "Yritys maksaa mainosmenoja tilisiirtona käyttöpankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.mainosmenot.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;

                        case tilinNimi.ostovelat:
                            Debug.Log("Tapahtuma nro. 15");
                            return "Yritys saa laskun mainosmenoista, joka kirjataan ostovelaksi. Laskun summa: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.mainosmenot.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                    }
                    break;
                case tilinNimi.vuokramenot:
                    switch (kreditKirjausTilinNimi)
                    {
                        case tilinNimi.kassatili:
                            Debug.Log("Tapahtuma nro. 16");
                            return "Yritys maksaa vuokramenoja käteisellä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.vuokramenot.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitili:
                            Debug.Log("Tapahtuma nro. 17");
                            return "Yritys maksaa vuokramenoja tilisiirtona pankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.vuokramenot.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitilikaytto:
                            Debug.Log("Tapahtuma nro. 18");
                            return "Yritys maksaa vuokramenoja tilisiirtona käyttöpankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.vuokramenot.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.ostovelat:
                            Debug.Log("Tapahtuma nro. 19");
                            return "Yritys saa laskun vuokramenoista, joka kirjataan ostovelaksi. Laskun summa: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.vuokramenot.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                    }
                    break;
                case tilinNimi.siivoustarvikkeet:
                    switch (kreditKirjausTilinNimi)
                    {
                        case tilinNimi.kassatili:
                            Debug.Log("Tapahtuma nro. 20");
                            return "Yritys maksaa siivoustarvikkeita käteisellä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.siivoustarvikkeet.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitili:
                            Debug.Log("Tapahtuma nro. 21");
                            return "Yritys maksaa siivoustarvikkeita tilisiirtona pankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.siivoustarvikkeet.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitilikaytto:
                            Debug.Log("Tapahtuma nro. 22");
                            return "Yritys maksaa siivoustarvikkeita tilisiirtona käyttöpankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.siivoustarvikkeet.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.ostovelat:
                            Debug.Log("Tapahtuma nro. 23");
                            return "Yritys saa laskun siivoustarvikkeista, joka kirjataan ostovelaksi. Laskun summa: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.siivoustarvikkeet.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                    }
                    break;
                case tilinNimi.korkomenot:
                    switch (kreditKirjausTilinNimi)
                    {

                        case tilinNimi.pankkitili:
                            Debug.Log("Tapahtuma nro. 24");
                            return "Yritys maksaa korkomenoja tilisiirtona pankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.korkomenot.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitilikaytto:
                            Debug.Log("Tapahtuma nro. 25");
                            return "Yritys maksaa korkomenoja tilisiirtona käyttöpankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.korkomenot.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break; 
                    }
                    break;
                case tilinNimi.verot:
                    switch (kreditKirjausTilinNimi)
                    {
                        case tilinNimi.pankkitili:
                            Debug.Log("Tapahtuma nro. 26");
                            return "Yritys maksaa veroja tilisiirtona pankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.verot.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitilikaytto:
                            Debug.Log("Tapahtuma nro. 27");
                            return "Yritys maksaa veroja tilisiirtona käyttöpankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.verot.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;

                    }
                    break;
                case tilinNimi.toimistotarvikkeet:
                    switch (kreditKirjausTilinNimi)
                    {
                        case tilinNimi.kassatili:
                            Debug.Log("Tapahtuma nro. 28");
                            return "Yritys maksaa toimistotarvikkeita käteisellä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.toimistotarvikkeet.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitili:
                            Debug.Log("Tapahtuma nro. 29");
                            return "Yritys maksaa toimistotarvikkeita tilisiirtona pankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.toimistotarvikkeet.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitilikaytto:
                            Debug.Log("Tapahtuma nro. 30");
                            return "Yritys maksaa toimistotarvikkeita käyttöpankkitililtä summalla" + summa.ToString().Replace(".", ",") + ". Tee debit kirjaus tilille " + tilinNimi.toimistotarvikkeet.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.ostovelat:
                            Debug.Log("Tapahtuma nro. 31");
                            return "Yritys saa laskun toimistotarvikkeista, joka kirjataan ostovelaksi. Laskun summa: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.toimistotarvikkeet.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                    }
                    break;
                case tilinNimi.ajoneuvomenot:
                    switch (kreditKirjausTilinNimi)
                    {
                        case tilinNimi.kassatili:
                            Debug.Log("Tapahtuma nro. 32");
                            return "Yritys maksaa ajoneuvokustannuksia käteisellä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.ajoneuvomenot.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitili:
                            Debug.Log("Tapahtuma nro. 33");
                            return "Yritys maksaa ajoneuvokustannuksia tilisiirtona pankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.ajoneuvomenot.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitilikaytto:
                            Debug.Log("Tapahtuma nro. 34");
                            return "Yritys maksaa ajoneuvokustannuksia tilisiirtona käyttöpankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.ajoneuvomenot.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.ostovelat:
                            Debug.Log("Tapahtuma nro. 35");
                            return "Yritys saa laskun ajoneuvokustannuksista, joka kirjataan ostovelaksi. Laskun summa: " + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.ajoneuvomenot.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                    }
                    break;
                //tästä eteenpäin kaikki rahatilejä
                case tilinNimi.kassatili:
                    //Ja casen sisään toinen switch jossa käsitellään sitten kreditkirjauksentilit. Rahatilille raha voi tulla tulotililtä, toiselta rahatilitlä tai pääomatililtä. Näissä ei siis käsitellä ollenkaan menotilejä
                    switch (kreditKirjausTilinNimi)
                    {
                        //rahatilit
                        case tilinNimi.pankkitili:
                            Debug.Log("Tapahtuma nro. 36");
                            return "Yritys nostaa rahaa kassaan pankkitililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.kassatili.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitilikaytto:
                            Debug.Log("Tapahtuma nro. 37");
                            return "Yritys nostaa rahaa kassaan käyttöpankkitililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.kassatili.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitilitalletus:
                            Debug.Log("Tapahtuma nro. 38");
                            return "Yritys nostaa rahaa kassaan talletustililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.kassatili.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        //tulotilit
                        case tilinNimi.myyntitili:
                            Debug.Log("Tapahtuma nro. 39");
                            return "Yritys saa myytyä tavaraa käteisellä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.kassatili.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.vuokratulot:
                            Debug.Log("Tapahtuma nro. 40");
                            return "Yritys saa vuokratuloja käteismaksuna. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.kassatili.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                    }
                    break;
                case tilinNimi.pankkitili:
                    switch (kreditKirjausTilinNimi)
                    {
                        //rahatilit
                        case tilinNimi.kassatili:
                            Debug.Log("Tapahtuma nro. 41");
                            return "Yritys siirtää rahaa pankkitilille kassasta. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.pankkitili.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitilikaytto:
                            Debug.Log("Tapahtuma nro. 42");
                            return "Yritys siirtää rahaa pankkitilille käyttöpankkitililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.pankkitili.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitilitalletus:
                            Debug.Log("Tapahtuma nro. 43");
                            return "Yritys siirtää rahaa pankkitilille talletustililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.pankkitili.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        //tulotilit
                        case tilinNimi.myyntitili:
                            Debug.Log("Tapahtuma nro. 44");
                            return "Yritys saa asiakkaalta maksusuorituksen tuotteesta pankkitilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.pankkitili.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.vuokratulot:
                            Debug.Log("Tapahtuma nro. 45");
                            return "Yritys saa vuokratuloja tilisiirtona pankkitilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.pankkitili.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.korkotulot:
                            Debug.Log("Tapahtuma nro. 46");
                            return "Yritys saa korkotuloja tilisiirtona pankkitilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.pankkitili.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.osinkotulot:
                            Debug.Log("Tapahtuma nro. 47");
                            return "Yritys saa osinkotuloja tilisiirtona pankkitilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.pankkitili.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.lainat:
                            Debug.Log("Tapahtuma nro. 48");
                            return "Yritys ottaa lainaa pankkitilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.pankkitili.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                    }
                    break;
                case tilinNimi.pankkitilikaytto:
                    switch (kreditKirjausTilinNimi)
                    {
                        //rahatilit
                        case tilinNimi.pankkitili:
                            Debug.Log("Tapahtuma nro. 49");
                            return "Yritys siirtää rahaa käyttöpankkitilille pankkitililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.pankkitilikaytto.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.kassatili:
                            Debug.Log("Tapahtuma nro. 50");
                            return "Yritys siirtää rahaa käyttöpankkitilille kassasta. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.pankkitilikaytto.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitilitalletus:
                            Debug.Log("Tapahtuma nro. 51");
                            return "Yritys siirtää rahaa käyttöpankkitilille talletustililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.pankkitilikaytto.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        //tulotilit
                        case tilinNimi.myyntitili:
                            Debug.Log("Tapahtuma nro. 52");
                            return "Yritys saa asiakkaalta maksusuorituksen tuotteesta käyttöpankkitilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.pankkitilikaytto.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.vuokratulot:
                            Debug.Log("Tapahtuma nro. 53");
                            return "Yritys saa vuokratuloja tilisiirtona käyttöpankkitilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.pankkitilikaytto.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.korkotulot:
                            Debug.Log("Tapahtuma nro. 54");
                            return "Yritys saa korkotuloja tilisiirtona käyttöpankkitilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.pankkitilikaytto.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.osinkotulot:
                            Debug.Log("Tapahtuma nro. 55");
                            return "Yritys saa osinkotuloja tilisiirtona käyttöpankkitilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.pankkitilikaytto.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.lainat:
                            Debug.Log("Tapahtuma nro. 56");
                            return "Yritys ottaa lainaa käyttöpankkitilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.pankkitilikaytto.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;

                    }
                    break;
                case tilinNimi.pankkitilitalletus:
                    switch (kreditKirjausTilinNimi)
                    {
                        //rahatilit
                        case tilinNimi.pankkitili:
                            Debug.Log("Tapahtuma nro. 57");
                            return "Yritys siirtää rahaa talletustilille pankkitililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.pankkitilitalletus.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitilikaytto:
                            Debug.Log("Tapahtuma nro. 58");
                            return "Yritys siirtää rahaa talletustilille käyttöpankkitililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.pankkitilitalletus.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.kassatili:
                            Debug.Log("Tapahtuma nro. 59");
                            return "Yritys siirtää rahaa talletustilille kassatililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.pankkitilitalletus.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        //tulotilit
                        case tilinNimi.myyntitili:
                            Debug.Log("Tapahtuma nro. 60");
                            return "Yritys saa asiakkaalta maksusuorituksen tuotteesta talletustilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.pankkitilitalletus.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.vuokratulot:
                            Debug.Log("Tapahtuma nro. 61");
                            return "Yritys saa vuokratuloja tilisiirtona talletustilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.pankkitilitalletus.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.korkotulot:
                            Debug.Log("Tapahtuma nro. 62");
                            return "Yritys saa korkotuloja tilisiirtona talletustilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.pankkitilitalletus.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.osinkotulot:
                            Debug.Log("Tapahtuma nro. 63");
                            return "Yritys saa osinkotuloja tilisiirtona talletustilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.pankkitilitalletus.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.lainat:
                            Debug.Log("Tapahtuma nro. 64");
                            return "Yritys ottaa lainaa talletustilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.pankkitilitalletus.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                    }
                    break;

                //tästä eteenpäin kaikki pääomatilejä
                case tilinNimi.yksityiskäyttö:
                    //Ja casen sisään toinen switch jossa käsitellään sitten kreditkirjauksentilit. Pääomatilille raha voi tulla rahatililtä eli kredit voi olla vain rahatilillä. Käydään siis rahatilit läpi
                    switch (kreditKirjausTilinNimi)
                    {
                        case tilinNimi.kassatili:
                            Debug.Log("Tapahtuma nro. 65");
                            return "Omistajan yksityisnosto kassasta. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.yksityiskäyttö.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break; 
                        case tilinNimi.pankkitili:
                            Debug.Log("Tapahtuma nro. 66");
                            return "Omistajan yksityisnosto pankkitililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.yksityiskäyttö.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitilikaytto:
                            Debug.Log("Tapahtuma nro. 67");
                            return "Omistajan yksityisnosto käyttöpankkitililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.yksityiskäyttö.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitilitalletus:
                            Debug.Log("Tapahtuma nro. 68");
                            return "Omistajan yksityisnosto talletustililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.yksityiskäyttö.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                    }
                    break;
                case tilinNimi.lainat:
                    switch (kreditKirjausTilinNimi)
                    {
                        case tilinNimi.kassatili:
                            Debug.Log("Tapahtuma nro. 69");
                            return "Yritys maksaa lainaa pois kassasta. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.lainat.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitili:
                            Debug.Log("Tapahtuma nro. 70");
                            return "Yritys maksaa lainaa pois tilisiirrolla käyttäen pankkitiliä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.lainat.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitilikaytto:
                            Debug.Log("Tapahtuma nro. 71");
                            return "Yritys maksaa lainaa pois tilisiirrolla käyttäen käyttöpankkitiliä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.lainat.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitilitalletus:
                            Debug.Log("Tapahtuma nro. 72");
                            return "Yrityksen maksaa lainaa pois käyttäen talletustiliä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.lainat.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                    }
                    break;
                case tilinNimi.omapääoma:
                    switch (kreditKirjausTilinNimi)
                    {
                        default:
                            Debug.Log("blaa!");
                            return "blaa";
                            break;
                    }
                    break;
                case tilinNimi.ostovelat:
                    switch (kreditKirjausTilinNimi)
                    {
                        case tilinNimi.kassatili:
                            Debug.Log("Tapahtuma nro. 73");
                            return "Yritys maksaa ostovelkaa käteisellä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.ostovelat.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitili:
                            Debug.Log("Tapahtuma nro. 74");
                            return "Yritys maksaa ostovelkaa käyttäen pankkitililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.ostovelat.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                        case tilinNimi.pankkitilikaytto:
                            Debug.Log("Tapahtuma nro. 75");
                            return "Yritys maksaa ostovelkaa käyttäen käyttöpankkitililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa. Tee debit kirjaus tilille " + tilinNimi.ostovelat.ToString() + " ja kredit kirjaus tilille " + kreditKirjausTilinNimi.ToString();
                            break;
                    }
                    break;
            }
            Debug.Log("blaa!");
            return "blaa";
        }
        else if (vaikeustaso == tehtavienKuvausVaikeusTaso.vaikea)
        {  //TÄSTÄ ALKAVAT VAIKEAT KUVAUKSET
            switch (debitKirjausTilinNimi)
            {
                //tästä eteenpäin kaikki menotilejä
                case tilinNimi.ostot:
                    //Ja casen sisään toinen switch jossa käsitellään sitten kreditkirjauksentilit. Kreditkirjauksia voi olla raha, pääoma ja tulotileillä. Ostoja ei voi kuitenkaan suoraan kattaa tulotileillä eli menotileille käydään läpi vain raha ja pääomatilit
                    switch (kreditKirjausTilinNimi)
                    {
                        case tilinNimi.kassatili:
                            //sisempien casejen sisällä tehdään nyt sitten lopullinen palautettava tapahtumakuvaus. Nyt on siis tiedossa minkä nimiseltä tililtä lähtee minkälainen kirjaus ja mille tilille
                            return "Yritys ostaa tavaraa käteisellä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitili:
                            return "Yritys ostaa tavaraa käyttäen pankkitiliä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitilikaytto:
                            return "Yritys ostaa tavaraa käyttäen käyttöpankkitiliä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.ostovelat:
                            return "Yritys ostaa tavaraa laskulla, joka merkitään ostovelaksi. Laskun summa: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                    }
                    break;
                case tilinNimi.palkat:
                    switch (kreditKirjausTilinNimi)
                    {
                        case tilinNimi.kassatili:
                            return "Yritys maksaa työntekijöiden palkkoja käteisellä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitili:
                            return "Yritys maksaa työntekijöiden palkkoja pankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitilikaytto:
                            return "Yritys maksaa työntekijöiden palkkoja käyttöpankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                    }
                    break;
                case tilinNimi.sähkömenot:
                    //return "Yritys maksaa sähkölaskun tililtä " + kreditKirjausTilinNimi.ToString() + " Tee debit kirjaus tilille " + tilinNimi.sähkömenot.ToString() + " ja kreditkirjaus tilille " + kreditKirjausTilinNimi.ToString();
                    switch (kreditKirjausTilinNimi)
                    {
                        case tilinNimi.kassatili:
                            return "Yritys maksaa sähkölaskun käteisellä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitili:
                            return "Yritys maksaa sähkölaskun tilisiirtona pankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitilikaytto:
                            return "Yritys maksaa sähkölaskun tilisiirtona käyttöpankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.ostovelat:
                            return "Yritys saa sähkölaskun, joka kirjataan ostovelaksi. Laskun summa: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                    }
                    break;
                case tilinNimi.mainosmenot:
                    //return "Yritys maksaa mainosmenoja tililtä " + kreditKirjausTilinNimi.ToString() + " Tee debit kirjaus tilille " + tilinNimi.mainosmenot.ToString() + " ja kreditkirjaus tilille " + kreditKirjausTilinNimi.ToString();
                    switch (kreditKirjausTilinNimi)
                    {
                        case tilinNimi.kassatili:
                            return "Yritys maksaa mainosmenoja käteisellä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitili:
                            return "Yritys maksaa mainosmenoja tilisiirtona pankkitililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitilikaytto:
                            return "Yritys maksaa mainosmenoja tilisiirtona käyttöpankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;

                        case tilinNimi.ostovelat:
                            return "Yritys saa laskun mainosmenoista, joka kirjataan ostovelaksi. Laskun summa: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                    }
                    break;
                case tilinNimi.vuokramenot:
                    switch (kreditKirjausTilinNimi)
                    {
                        case tilinNimi.kassatili:
                            return "Yritys maksaa vuokramenoja käteisellä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitili:
                            return "Yritys maksaa vuokramenoja tilisiirtona pankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitilikaytto:
                            return "Yritys maksaa vuokramenoja tilisiirtona käyttöpankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.ostovelat:
                            return "Yritys saa laskun vuokramenoista, joka kirjataan ostovelaksi. Laskun summa: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                    }
                    break;
                case tilinNimi.siivoustarvikkeet:
                    switch (kreditKirjausTilinNimi)
                    {
                        case tilinNimi.kassatili:
                            return "Yritys maksaa siivoustarvikkeita käteisellä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitili:
                            return "Yritys maksaa siivoustarvikkeita tilisiirtona pankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitilikaytto:
                            return "Yritys maksaa siivoustarvikkeita tilisiirtona käyttöpankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.ostovelat:
                            return "Yritys saa laskun siivoustarvikkeista, joka kirjataan ostovelaksi. Laskun summa: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                    }
                    break;
                case tilinNimi.korkomenot:
                    switch (kreditKirjausTilinNimi)
                    {

                        case tilinNimi.pankkitili:
                            return "Yritys maksaa korkomenoja tilisiirtona pankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitilikaytto:
                            return "Yritys maksaa korkomenoja tilisiirtona käyttöpankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                    }
                    break;
                case tilinNimi.verot:
                    switch (kreditKirjausTilinNimi)
                    {
                        case tilinNimi.pankkitili:
                            return "Yritys maksaa veroja tilisiirtona pankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitilikaytto:
                            return "Yritys maksaa veroja tilisiirtona käyttöpankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;

                    }
                    break;
                case tilinNimi.toimistotarvikkeet:
                    switch (kreditKirjausTilinNimi)
                    {
                        case tilinNimi.kassatili:
                            return "Yritys maksaa toimistotarvikkeita käteisellä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitili:
                            return "Yritys maksaa toimistotarvikkeita tilisiirtona pankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitilikaytto:
                            return "Yritys maksaa toimistotarvikkeita käyttöpankkitililtä summalla" + summa.ToString().Replace(".", ",") + ".";
                            break;
                        case tilinNimi.ostovelat:
                            return "Yritys saa laskun toimistotarvikkeista, joka kirjataan ostovelaksi. Laskun summa: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                    }
                    break;
                case tilinNimi.ajoneuvomenot:
                    switch (kreditKirjausTilinNimi)
                    {
                        case tilinNimi.kassatili:
                            return "Yritys maksaa ajoneuvokustannuksia käteisellä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitili:
                            return "Yritys maksaa ajoneuvokustannuksia tilisiirtona pankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitilikaytto:
                            return "Yritys maksaa ajoneuvokustannuksia tilisiirtona käyttöpankkitililtä. Rahamäärä: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.ostovelat:
                            return "Yritys saa laskun ajoneuvokustannuksista, joka kirjataan ostovelaksi. Laskun summa: " + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                    }
                    break;
                //tästä eteenpäin kaikki rahatilejä
                case tilinNimi.kassatili:
                    //Ja casen sisään toinen switch jossa käsitellään sitten kreditkirjauksentilit. Rahatilille raha voi tulla tulotililtä, toiselta rahatilitlä tai pääomatililtä. Näissä ei siis käsitellä ollenkaan menotilejä
                    switch (kreditKirjausTilinNimi)
                    {
                        //rahatilit
                        case tilinNimi.pankkitili:
                            return "Yritys nostaa rahaa kassaan pankkitililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitilikaytto:
                            return "Yritys nostaa rahaa kassaan käyttöpankkitililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitilitalletus:
                            return "Yritys nostaa rahaa kassaan talletustililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        //tulotilit
                        case tilinNimi.myyntitili:
                            return "Yritys saa myytyä tavaraa käteisellä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.vuokratulot:
                            return "Yritys saa vuokratuloja käteismaksuna. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                    }
                    break;
                case tilinNimi.pankkitili:
                    switch (kreditKirjausTilinNimi)
                    {
                        //rahatilit
                        case tilinNimi.kassatili:
                            return "Yritys siirtää rahaa pankkitilille kassasta. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitilikaytto:
                            return "Yritys siirtää rahaa pankkitilille käyttöpankkitililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitilitalletus:
                            return "Yritys siirtää rahaa pankkitilille talletustililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        //tulotilit
                        case tilinNimi.myyntitili:
                            return "Yritys saa asiakkaalta maksusuorituksen tuotteesta pankkitilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.vuokratulot:
                            return "Yritys saa vuokratuloja tilisiirtona pankkitilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.korkotulot:
                            return "Yritys saa korkotuloja tilisiirtona pankkitilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.osinkotulot:
                            return "Yritys saa osinkotuloja tilisiirtona pankkitilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.lainat:
                            return "Yritys ottaa lainaa pankkitilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                    }
                    break;
                case tilinNimi.pankkitilikaytto:
                    switch (kreditKirjausTilinNimi)
                    {
                        //rahatilit
                        case tilinNimi.pankkitili:
                            return "Yritys siirtää rahaa käyttöpankkitilille pankkitililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.kassatili:
                            return "Yritys siirtää rahaa käyttöpankkitilille kassasta. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitilitalletus:
                            return "Yritys siirtää rahaa käyttöpankkitilille talletustililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        //tulotilit
                        case tilinNimi.myyntitili:
                            return "Yritys saa asiakkaalta maksusuorituksen tuotteesta käyttöpankkitilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.vuokratulot:
                            return "Yritys saa vuokratuloja tilisiirtona käyttöpankkitilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.korkotulot:
                            return "Yritys saa korkotuloja tilisiirtona käyttöpankkitilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.osinkotulot:
                            return "Yritys saa osinkotuloja tilisiirtona käyttöpankkitilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.lainat:
                            return "Yritys ottaa lainaa käyttöpankkitilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;

                    }
                    break;
                case tilinNimi.pankkitilitalletus:
                    switch (kreditKirjausTilinNimi)
                    {
                        //rahatilit
                        case tilinNimi.pankkitili:
                            return "Yritys siirtää rahaa talletustilille pankkitililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitilikaytto:
                            return "Yritys siirtää rahaa talletustilille käyttöpankkitililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.kassatili:
                            return "Yritys siirtää rahaa talletustilille kassatililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        //tulotilit
                        case tilinNimi.myyntitili:
                            return "Yritys saa asiakkaalta maksusuorituksen tuotteesta talletustilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.vuokratulot:
                            return "Yritys saa vuokratuloja tilisiirtona talletustilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.korkotulot:
                            return "Yritys saa korkotuloja tilisiirtona talletustilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.osinkotulot:
                            return "Yritys saa osinkotuloja tilisiirtona talletustilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.lainat:
                            return "Yritys ottaa lainaa talletustilille. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                    }
                    break;

                //tästä eteenpäin kaikki pääomatilejä
                case tilinNimi.yksityiskäyttö:
                    //Ja casen sisään toinen switch jossa käsitellään sitten kreditkirjauksentilit. Pääomatilille raha voi tulla rahatililtä eli kredit voi olla vain rahatilillä. Käydään siis rahatilit läpi
                    switch (kreditKirjausTilinNimi)
                    {
                        case tilinNimi.kassatili:
                            return "Omistajan yksityisnosto kassasta. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitili:
                            return "Omistajan yksityisnosto pankkitililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitilikaytto:
                            return "Omistajan yksityisnosto käyttöpankkitililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitilitalletus:
                            return "Omistajan yksityisnosto talletustililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                    }
                    break;
                case tilinNimi.lainat:
                    switch (kreditKirjausTilinNimi)
                    {
                        case tilinNimi.kassatili:
                            return "Yritys maksaa lainaa pois kassasta. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitili:
                            return "Yritys maksaa lainaa pois tilisiirrolla käyttäen pankkitiliä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitilikaytto:
                            return "Yritys maksaa lainaa pois tilisiirrolla käyttäen käyttöpankkitiliä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitilitalletus:
                            return "Yrityksen maksaa lainaa pois käyttäen talletustiliä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                    }
                    break;
                case tilinNimi.omapääoma:
                    switch (kreditKirjausTilinNimi)
                    {
                        default:
                            Debug.Log("Tänne ei pitäisi mennä!");
                            return "blaa";
                            break;
                    }
                    break;
                case tilinNimi.ostovelat:
                    switch (kreditKirjausTilinNimi)
                    {
                        case tilinNimi.kassatili:
                            return "Yritys maksaa ostovelkaa käteisellä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitili:
                            return "Yritys maksaa ostovelkaa käyttäen pankkitililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                        case tilinNimi.pankkitilikaytto:
                            return "Yritys maksaa ostovelkaa käyttäen käyttöpankkitililtä. Rahamäärä:" + summa.ToString().Replace(".", ",") + " euroa.";
                            break;
                    }
                    break;
            }
            Debug.Log("blaa!");
            return "blaa";
        }
        else {
            Debug.Log("blaa!");
            return "blaa";
        }
    }
}
