﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneSelectorScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameObject.FindGameObjectWithTag("PisteText").GetComponent<Text>().text = "Pisteesi: " + PisteenLaskuJaAchievementManager.instance.nykyisetPisteet;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void LoadScene(int sceneNumber) {
        SceneManager.LoadScene(sceneNumber);
    }

    public void LopetaOhjelma() {
        Application.Quit();
    }
}
