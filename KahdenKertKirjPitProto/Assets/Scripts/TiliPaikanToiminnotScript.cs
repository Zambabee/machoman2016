﻿using UnityEngine;
using System.Collections;

public class TiliPaikanToiminnotScript : MonoBehaviour {

    private GameObject paikallaOlevaTili1;

    public GameObject PaikallaOlevaTili1
    {
        get { return paikallaOlevaTili1; }
        set { paikallaOlevaTili1 = value; }
    }
    private GameObject paikallaOlevaTili2;

    public GameObject PaikallaOlevaTili2
    {
        get { return paikallaOlevaTili2; }
        set { paikallaOlevaTili2 = value; }
    }


    public GameObject tilinNakyvaPaikka1;
    public GameObject tilinNakyvaPaikka2;
    public GameObject tilinPiilossaOlevaPaikka;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void VieTiliPiiloon(int paikka) {
        if (paikka == 0)
        {
            paikallaOlevaTili1.transform.position = tilinPiilossaOlevaPaikka.transform.position;
        }
        else if(paikka == 1){
            PaikallaOlevaTili2.transform.position = tilinPiilossaOlevaPaikka.transform.position;
        }
    }

    public void TuoTiliEsille(int paikka,GameObject tili) {
        if (paikka == 0)
        {
            paikallaOlevaTili1 = tili;
            tili.transform.position = tilinNakyvaPaikka1.transform.position;
        }
        else if (paikka == 1)
        {
            paikallaOlevaTili2 = tili;
            tili.transform.position = tilinNakyvaPaikka2.transform.position;
        }
    }
}
