﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameLogicScript : MonoBehaviour {

    public Text infoTeksti;
    public Text kuvausTeksti;
    public Text tapahtumienMaaraText;
    public int vaiheitaYht;
    public int kirjauksiaTehtyTapahtumaan = 0;
    public GameObject VoittoCanvas;
    public Button tarkistaKirjauksetNappi;
    public Button tyhjennaKierroksenKirjauksetNappi;
    public GameObject oikeinKuva;
    public GameObject vaarinKuva;
    private int nykyinenVaihe;



	// Use this for initialization
	IEnumerator Start () {
        yield return new WaitForSeconds(0.5f);

            yield return new WaitForEndOfFrame();
            vaiheitaYht = SceneCreatorScript.instance.LiikeTapahtumat.Count;
            nykyinenVaihe = 0;
            kuvausTeksti.text = SceneCreatorScript.instance.LiikeTapahtumat[nykyinenVaihe].tapahtumaKuvaus;
            tapahtumienMaaraText.text = (nykyinenVaihe+1) +"/"+ SceneCreatorScript.instance.LiikeTapahtumat.Count;
	}

   /* void Start() {
        vaiheitaYht = SceneCreatorScript.instance.LiikeTapahtumat.Count;
        nykyinenVaihe = 0;
        kuvausTeksti.text = SceneCreatorScript.instance.LiikeTapahtumat[nykyinenVaihe].tapahtumaKuvaus;
        tapahtumienMaaraText.text = (nykyinenVaihe + 1) + "/" + SceneCreatorScript.instance.LiikeTapahtumat.Count;
    }*/
	
	// Update is called once per frame
	void Update () {
	
	}

    public void TestiSeuraavaVaihde() {
        if (nykyinenVaihe < (vaiheitaYht-1))
        {
            nykyinenVaihe++;
            kuvausTeksti.text = SceneCreatorScript.instance.LiikeTapahtumat[nykyinenVaihe].tapahtumaKuvaus;
        }
        else {
            kuvausTeksti.text = "Liiketapahtumat loppuivat. Onnittelut tänne asti pääsystä!";
        }
        

    }

    public void TarkistaOnkoKirjauksetOikein() {
        bool oikein = false;
        kirjauksiaTehtyTapahtumaan = 0;
        //OTetaan ensin muuttujiin talteen oikeat kirjaustiedot
        Kirjaus debitKirjaus = SceneCreatorScript.instance.LiikeTapahtumat[nykyinenVaihe].debitKirjaus;
        Kirjaus kreditKirjaus = SceneCreatorScript.instance.LiikeTapahtumat[nykyinenVaihe].kreditKirjaus;

        //Sitten etsitään kentältä kaikki tilit taulukkoon
        GameObject[] kaikkiTilit = GameObject.FindGameObjectsWithTag("Tili");
        //Otetaan kahteen muuttujaan talteen tilit joille kirjaukset olisi pitänyt tehdä id:n perusteella
        GameObject debitTili = new GameObject();
        GameObject kreditTili = new GameObject();
        for (int i = 0; i < kaikkiTilit.Length; i++)
        {
            if (kaikkiTilit[i].GetComponent<TilinScript>().tilinId == debitKirjaus.kirjausTilinId) {
                debitTili = kaikkiTilit[i];
            }
            else if (kaikkiTilit[i].GetComponent<TilinScript>().tilinId == kreditKirjaus.kirjausTilinId) {
                kreditTili = kaikkiTilit[i];
            }
        }

        //Debug.Log("Debit kirjauksen pitäisi olla tilillä id : " + debitKirjaus.kirjausTilinId + " ja arvona pitäisi olla " + debitKirjaus.kirjauksenArvo);
        //Debug.Log("Tätä tiliä vastaavalla tilillä jonka nimi on " + debitTili.GetComponent<TilinScript>().tilinNimi + " on merkittynä debit kohtaan arvo " + debitTili.GetComponent<TilinScript>().debits[debitTili.GetComponent<TilinScript>().CurrentIndex].text);

        //Debug.Log("Kredit kirjauksen pitäisi olla tilillä id : " + kreditKirjaus.kirjausTilinId + " ja arvona pitäisi olla " + kreditKirjaus.kirjauksenArvo);
        //Debug.Log("Tätä tiliä vastaavalla tilillä jonka nimi on " + kreditTili.GetComponent<TilinScript>().tilinNimi + " on merkittynä krebit kohtaan arvo " + kreditTili.GetComponent<TilinScript>().kredits[kreditTili.GetComponent<TilinScript>().CurrentIndex].text);
        //float debitArvo = float.Parse(debitTili.GetComponent<TilinScript>().debits[debitTili.GetComponent<TilinScript>().CurrentIndex].text);
        //float kreditArvo = float.Parse(kreditTili.GetComponent<TilinScript>().kredits[kreditTili.GetComponent<TilinScript>().CurrentIndex].text);
        float debitArvo = 0;
        float kreditArvo = 0;
        //tilin inputfieldissä on nyt tällä hetkellä merkattuna pilkku, muutetaan pilkku pisteeksi jotta se voidaan parsia oikeaan muotoon
        string parsittavaDebit = debitTili.GetComponent<TilinScript>().debits[debitTili.GetComponent<TilinScript>().CurrentIndex].text.Replace(",",".");
        string parsittavaKredit = kreditTili.GetComponent<TilinScript>().kredits[kreditTili.GetComponent<TilinScript>().CurrentIndex].text.Replace(",",".");
        float.TryParse(parsittavaDebit,out debitArvo);
        float.TryParse(parsittavaKredit, out kreditArvo);

        Debug.Log("Debit kirjauksen pitäisi olla tilillä id : " + debitKirjaus.kirjausTilinId + " ja arvona pitäisi olla " + debitKirjaus.kirjauksenArvo);
        Debug.Log("Debitkirjauksen arvo on: " + debitArvo);
        Debug.Log("Kredit kirjauksen pitäisi olla tilillä id : " + kreditKirjaus.kirjausTilinId + " ja arvona pitäisi olla " + kreditKirjaus.kirjauksenArvo);
        Debug.Log("Kreditkirjauksen arvo on: " + kreditArvo);

        if (debitKirjaus.kirjauksenArvo == debitArvo && kreditArvo == kreditKirjaus.kirjauksenArvo)
        {
            oikeinKuva.SetActive(true);
            vaarinKuva.SetActive(false);
            PisteenLaskuJaAchievementManager.instance.KasvataPisteita();
            oikein = true;
            Debug.Log("Kirjaus oli oikein!");
            debitTili.GetComponent<TilinScript>().CurrentIndex++;
            kreditTili.GetComponent<TilinScript>().CurrentIndex++;
            //tarkistetaan onko tilin currentindex arvo yli 4, jos on niin merkataan currentindexin arvoksi 0 eli tyhjennetään tili ja ruvetaan täyttämään sitä alusta.
            if (debitTili.GetComponent<TilinScript>().CurrentIndex > 4)
            {
                debitTili.GetComponent<TilinScript>().CurrentIndex = 0;
                debitTili.GetComponent<TilinScript>().TyhjennaTilinNakyvatKirjaukset();
            }
            if (kreditTili.GetComponent<TilinScript>().CurrentIndex > 4)
            {
                kreditTili.GetComponent<TilinScript>().CurrentIndex = 0;
                kreditTili.GetComponent<TilinScript>().TyhjennaTilinNakyvatKirjaukset();
            }
            //Kutsutaan arvo muuttunut funktiota kaikille tileille jotta niiden syöttökentät aukeavat, ne ovat muuten vielä kiinni RekisteroiKirjauksen jäljiltä
            for (int i = 0; i < kaikkiTilit.Length; i++)
            {
                kaikkiTilit[i].GetComponent<TilinScript>().ArvoMuuttunut();
            }
            //debitTili.GetComponent<TilinScript>().ArvoMuuttunut();
            //kreditTili.GetComponent<TilinScript>().ArvoMuuttunut();
            StartCoroutine(NaytaInfoTeksti(2.5f,"Kirjaukset olivat oikein"));
            if (nykyinenVaihe < (vaiheitaYht - 1))
            {
                nykyinenVaihe++;
                kuvausTeksti.text = SceneCreatorScript.instance.LiikeTapahtumat[nykyinenVaihe].tapahtumaKuvaus;
                tapahtumienMaaraText.text = (nykyinenVaihe+1) + "/" + SceneCreatorScript.instance.LiikeTapahtumat.Count;
            }
            else
            {
                VoittoCanvas.gameObject.SetActive(true);
                gameObject.SetActive(false);
                kuvausTeksti.text = "Liiketapahtumat loppuivat. Onnittelut tänne asti pääsystä!";
            }
            //Asetetaan kirjauksien tarkistusnappi epäaktiiviseksi
            tarkistaKirjauksetNappi.GetComponent<Button>().interactable = false;
            //Asetetaan myös tyhjennysnappi epäaktiiviseksi
            tyhjennaKierroksenKirjauksetNappi.GetComponent<Button>().interactable = false;
        }
        else {
            oikeinKuva.SetActive(false);
            vaarinKuva.SetActive(true);
            PisteenLaskuJaAchievementManager.instance.VahennaPisteita();
            oikein = false;
            Debug.Log("Kirjaus oli virheellinen!");
            StartCoroutine(NaytaInfoTeksti(2.5f,"Kirjaukset olivat väärin. Yritä uudestaan."));
            //Avataan uudestaan kaikkien tilien sen hetkisen indeksin kirjauskohdat
            for (int i = 0; i < kaikkiTilit.Length; i++)
            {
                kaikkiTilit[i].GetComponent<TilinScript>().AvaaEdellisetKirjaukset();
            }
            //Asetetaan kirjauksien tarkistusnappi epäaktiiviseksi
            tarkistaKirjauksetNappi.GetComponent<Button>().interactable = false;
            //Asetetaan myös tyhjennysnappi epäaktiiviseksi
            tyhjennaKierroksenKirjauksetNappi.GetComponent<Button>().interactable = false;
        }
        //Riippumatta oliko oikein vai väärin niin asetetaan tilien kirjausnapit aktiiviseksi
        for (int i = 0; i < kaikkiTilit.Length; i++)
        {
            kaikkiTilit[i].GetComponent<TilinScript>().AsetaKirjausNappiKayttoon();
        }
    }

    //coroutine virheen näyttämiseen
    IEnumerator NaytaInfoTeksti(float nayttoAika, string teksti) {
        infoTeksti.text = teksti;
        infoTeksti.gameObject.SetActive(true);
        yield return new WaitForSeconds(nayttoAika);
        infoTeksti.gameObject.SetActive(false);
    }

    public void RekisteroiKirjaus() {
        kirjauksiaTehtyTapahtumaan++;
        if (kirjauksiaTehtyTapahtumaan == 2) {
            //Avataan tarkistusnappi
            tarkistaKirjauksetNappi.GetComponent<Button>().interactable = true;
            GameObject[] kaikkiTilit = GameObject.FindGameObjectsWithTag("Tili");
            for (int i = 0; i < kaikkiTilit.Length; i++)
            {
                kaikkiTilit[i].GetComponent<TilinScript>().debits[kaikkiTilit[i].GetComponent<TilinScript>().CurrentIndex].interactable = false;
                kaikkiTilit[i].GetComponent<TilinScript>().kredits[kaikkiTilit[i].GetComponent<TilinScript>().CurrentIndex].interactable = false;
            }
        }
        else if (kirjauksiaTehtyTapahtumaan > 0) { 
           //Avataan kirjausten perumisnappi
            tyhjennaKierroksenKirjauksetNappi.GetComponent<Button>().interactable = true;
        }
    }

    public void tyhjennaKierroksenKirjaukset() { //Metodi jota kutsutaan kun halutaankin perua kierroksen kirjaukset
        //etsitään kaikki tilit ja kutsutaan jokaisen tilinscriptin AvaaEdellisetKirjaukset-metodia
        GameObject[] kaikkiTilit = GameObject.FindGameObjectsWithTag("Tili");
        for (int i = 0; i < kaikkiTilit.Length; i++)
        {
            kaikkiTilit[i].GetComponent<TilinScript>().AvaaEdellisetKirjaukset();
            //Avataan myös kirjausnappi
            kaikkiTilit[i].GetComponent<TilinScript>().AsetaKirjausNappiKayttoon();
        }
        //Merkataan tehtyjen kirjausten määrä nollaksi
        kirjauksiaTehtyTapahtumaan = 0;
        //Asetetaan kirjauksien tarkistusnappi epäaktiiviseksi
        tarkistaKirjauksetNappi.GetComponent<Button>().interactable = false;
        //Asetetaan myös tyhjennysnappi epäaktiiviseksi
        tyhjennaKierroksenKirjauksetNappi.GetComponent<Button>().interactable = false;
    }

    public void PalaaPaaValikkoon() {
        SceneManager.LoadScene(0);
    }

    public void MeneSeuraavaanKenttaan() {
        if (SceneManager.GetActiveScene().buildIndex < SceneManager.sceneCountInBuildSettings)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        else {
            SceneManager.LoadScene(0);
        }
    }
}
