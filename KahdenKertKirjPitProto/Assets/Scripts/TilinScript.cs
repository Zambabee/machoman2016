﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TilinScript : MonoBehaviour {

    public int tilinId;
    public InputField[] debits;
    public InputField[] kredits;
    public Text tilinNimiUiText;
    public Text debitUiText;
    public Text kreditUiText;
    public Button kirjausNappi;

    public tilinTyyppi tyyppi;
    public tilinNimi tilinNimiEnum;
    public string tilinNimi;

    private int currentIndex;

    public int CurrentIndex
    {
        get { return currentIndex; }
        set { currentIndex = value; }
    }


	// Use this for initialization
	void Start () {
        kirjausNappi.interactable = true;
        currentIndex = 0;
        for (int i = 0; i < debits.Length; i++)
        {
            debits[i].interactable = false;
        }
        for (int i = 0; i < kredits.Length; i++)
        {
            kredits[i].interactable = false;
        }
        debits[0].interactable = true;
        kredits[0].interactable = true;
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    public void KirjaaTapahtuma() {
        if (debits[currentIndex].text != "" || kredits[currentIndex].text != "")
        {
            for (int i = 0; i < debits.Length; i++)
            {

                if (debits[i].IsInteractable())
                {
                    debits[i].interactable = false;
                    if (debits[i].text != "")
                    {
                        Debug.Log("Debitin sisältö oli: " + debits[i].text);
                    }
                    if (i < debits.Length - 1)
                    {
                        //debits[i + 1].interactable = true;
                    }
                    break;
                }
            }
            for (int i = 0; i < kredits.Length; i++)
            {
                if (kredits[i].IsInteractable())
                {
                    kredits[i].interactable = false;
                    if (i < kredits.Length - 1)
                    {
                        //kredits[i + 1].interactable = true;
                    }
                    break;
                }
            }
            if (GameObject.FindGameObjectWithTag("Canvas"))
            {
                GameObject.FindGameObjectWithTag("Canvas").GetComponent<GameLogicScript>().RekisteroiKirjaus();
            }
            //currentIndex++;
            //ArvoMuuttunut();
            PoistaKirjausNappiKaytosta();
        }
    }

    public void ArvoMuuttunut() {
        if (debits[currentIndex].text == "" && kredits[currentIndex].text != "") {
            debits[currentIndex].interactable = false;
            kredits[currentIndex].interactable = true;
        }
        else if (debits[currentIndex].text != "" && kredits[currentIndex].text == "")
        {
            debits[currentIndex].interactable = true;
            kredits[currentIndex].interactable = false;
        }
        else {
            debits[currentIndex].interactable = true;
            kredits[currentIndex].interactable = true;
        }
    }

    public void TyhjennaTilinNakyvatKirjaukset() {
        for (int i = 0; i < debits.Length; i++)
        {
            debits[i].text = "";
        }
        for (int i = 0; i < kredits.Length; i++)
        {
            kredits[i].text = "";
        }
    }

    public void PoistaKirjausNappiKaytosta() {
        kirjausNappi.interactable = false;
    }
    public void AsetaKirjausNappiKayttoon() {
        kirjausNappi.interactable = true;
    }

    public void AlustaTilinTiedot() {  //Kutsu tätä sen jälkeen kun tilin nimi ja tilin tyyppi on asetettu!
        //tilinNimiUiText.text = tilinNimi;
        tilinNimiUiText.text = tilinNimiEnum.ToString();
        if (tyyppi == tilinTyyppi.tulotili)
        {
            debitUiText.text = "Debit -";
            kreditUiText.text = "Kredit +";
        }
        else if(tyyppi == tilinTyyppi.menotili){
            debitUiText.text = "Debit +";
            kreditUiText.text = "Kredit -";
        }
        else if (tyyppi == tilinTyyppi.rahatili) {
            debitUiText.text = "Debit +";
            kreditUiText.text = "Kredit -";
        }
        else if (tyyppi == tilinTyyppi.paaomatili) {
            debitUiText.text = "Debit -";
            kreditUiText.text = "Kredit +";
        }
    }

    public void AvaaEdellisetKirjaukset() {
        debits[currentIndex].text = "";
        kredits[currentIndex].text = "";
        debits[currentIndex].interactable = true;
        kredits[currentIndex].interactable = true;
    }
}
