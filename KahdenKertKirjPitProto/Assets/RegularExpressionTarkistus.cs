﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text.RegularExpressions;

public class RegularExpressionTarkistus : MonoBehaviour
{

    //Set thius to true if you want to use commas instead of fullstops
    public bool useComma = false;



    /// <summary>
    /// This will get called by the OnTextChanged Event
    /// Make sure you use the Dynamic Text version not the verison that will let you edit the string
    /// </summary>
    /// <param name="input"></param>
    public void ValidateStringCharacters(string input)
    {
        //get out input feild, even though we just fired an event from it
        InputField InputField = GetComponent<InputField>();

        //assign our regular expression string, it will be different if you use commas hence the "if"
        string regexp = @"[^0-9\.]?";
        if (useComma)
        {
            //commas don't neeed escaping
            regexp = @"[^0-9,]?";
        }

        //Assign out regex Object
        Regex regex = new Regex(regexp);

        //Replace anything that isn't a number with empty
        input = regex.Replace(input, "");


        //print(input);
        //Apply our Validated string
        InputField.text = input;

    }
}