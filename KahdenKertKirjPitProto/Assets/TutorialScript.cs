﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[System.Serializable]
public struct laatikonPaikkaJaKoko{
    public Vector3 position;
    public Vector3 scale;
    public Vector3 nuolenPosition;
    public Vector3 nuolenRotation;
}

public class TutorialScript : MonoBehaviour {
    public GameObject tutoriaaliLaatikko;
    [SerializeField]
    public laatikonPaikkaJaKoko aloitusLaatikonKoko;
    [SerializeField]
    public laatikonPaikkaJaKoko vaihe1LaatikonKokoJaPaikka;
    public laatikonPaikkaJaKoko vaihe2LaatikonKokoJaPaikka;
    public laatikonPaikkaJaKoko vaihe3LaatikonKokoJaPaikka;
    public laatikonPaikkaJaKoko vaihe4LaatikonKokoJaPaikka;
    public laatikonPaikkaJaKoko vaihe5LaatikonKokoJaPaikka;
    public laatikonPaikkaJaKoko vaihe6LaatikonKokoJaPaikka;
    public laatikonPaikkaJaKoko vaihe7LaatikonKokoJaPaikka;
    public GameObject nuoli;
    public Button seuraavaVaiheNappi;
    public Text selitysTeksti;
    public Button perumisNappi;
    public Button tarkistaKirjauksetNappi;
    public InputField pankkitiliInput1;
    public InputField pankkitiliInput2;
    public Button pankkitiliKirjausNappi;
    public InputField ostotiliInput1;
    public InputField ostotiliInput2;
    public Button ostotiliKirjausNappi;
    public Text oikeinTeksti;
    public Text pisteTeksti;

    private int tutoriaalinVaihe = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        switch (tutoriaalinVaihe) { 
            case 0:
                selitysTeksti.text = "Tervetuloa tutoriaaliin. Eteenpäin pääset tekemällä annetun tehtävän tai painamalla Ok-nappia. Päävalikkoon voit palata yläoikeasta kulmasta";
                tutoriaaliLaatikko.GetComponent<RectTransform>().localPosition = aloitusLaatikonKoko.position;
                tutoriaaliLaatikko.GetComponent<RectTransform>().localScale = aloitusLaatikonKoko.scale;
                pankkitiliInput1.interactable = false;
                pankkitiliInput2.interactable = false;
                pankkitiliKirjausNappi.interactable = false;
                ostotiliInput1.interactable = false;
                ostotiliInput2.interactable = false;
                ostotiliKirjausNappi.interactable = false;
                seuraavaVaiheNappi.interactable = true;
                nuoli.SetActive(false);
                break;
            case 1:
                selitysTeksti.text = "Tähän tulee aina tehtäväkuvaus. Sinun tulee täyttää tilitapahtuman tiedot tämän kuvauksen pohjalta";
                tutoriaaliLaatikko.GetComponent<RectTransform>().localPosition= vaihe1LaatikonKokoJaPaikka.position;
                tutoriaaliLaatikko.GetComponent<RectTransform>().localScale = vaihe1LaatikonKokoJaPaikka.scale;
                nuoli.GetComponent<RectTransform>().localPosition = vaihe1LaatikonKokoJaPaikka.nuolenPosition;
                nuoli.GetComponent<RectTransform>().localRotation = Quaternion.Euler(vaihe1LaatikonKokoJaPaikka.nuolenRotation);
                //nuoli.GetComponent<RectTransform>().localRotation.SetEulerAngles(vaihe1LaatikonKokoJaPaikka.nuolenRotation);
                nuoli.SetActive(true);
                seuraavaVaiheNappi.interactable = true;
                break;
            case 2:

                selitysTeksti.text = "Täytä kuvauksen mukainen kredit-summa. Kun olet syöttänyt luvun paina \"Kirjaa tapahtuma\"-nappia";
                tutoriaaliLaatikko.GetComponent<RectTransform>().localPosition= vaihe2LaatikonKokoJaPaikka.position;
                tutoriaaliLaatikko.GetComponent<RectTransform>().localScale = vaihe2LaatikonKokoJaPaikka.scale;
                nuoli.GetComponent<RectTransform>().localPosition = vaihe2LaatikonKokoJaPaikka.nuolenPosition;
                nuoli.GetComponent<RectTransform>().localRotation = Quaternion.Euler(vaihe2LaatikonKokoJaPaikka.nuolenRotation);
                //nuoli.GetComponent<RectTransform>().localRotation.SetEulerAngles(vaihe2LaatikonKokoJaPaikka.nuolenRotation);
                seuraavaVaiheNappi.gameObject.SetActive(false);
                pankkitiliInput1.interactable = true;
                float pankkitilinArvo = 0;
                float.TryParse(pankkitiliInput1.text.Replace(",","."),out pankkitilinArvo);
                Debug.Log("Syötetty arvo pankkitilille on: " + pankkitilinArvo);
                if (pankkitilinArvo == 125.39f) {
                    pankkitiliKirjausNappi.interactable = true;
                }
                else if (pankkitilinArvo != 125.39) {
                    pankkitiliKirjausNappi.interactable = false;
                }
                break;
            case 3:

                selitysTeksti.text = "Kun olet tehnyt yhden tai kaksi kirjausta niin tämä nappi aukeaa ja voit perua sillä kirjauksesi menettämättä pisteitä";
                tutoriaaliLaatikko.GetComponent<RectTransform>().localPosition= vaihe3LaatikonKokoJaPaikka.position;
                tutoriaaliLaatikko.GetComponent<RectTransform>().localScale = vaihe3LaatikonKokoJaPaikka.scale;
                nuoli.GetComponent<RectTransform>().localPosition = vaihe3LaatikonKokoJaPaikka.nuolenPosition;
                nuoli.GetComponent<RectTransform>().localRotation = Quaternion.Euler(vaihe3LaatikonKokoJaPaikka.nuolenRotation);
                //nuoli.GetComponent<RectTransform>().localRotation.SetEulerAngles(vaihe3LaatikonKokoJaPaikka.nuolenRotation);
                seuraavaVaiheNappi.interactable = true;
               
                break;
            case 4:

                selitysTeksti.text = "Täytä kuvauksen mukainen dedit-summa. Kun olet syöttänyt luvun paina \"Kirjaa tapahtuma\"-nappia";
                tutoriaaliLaatikko.GetComponent<RectTransform>().localPosition= vaihe4LaatikonKokoJaPaikka.position;
                tutoriaaliLaatikko.GetComponent<RectTransform>().localScale = vaihe4LaatikonKokoJaPaikka.scale;
                nuoli.GetComponent<RectTransform>().localPosition = vaihe4LaatikonKokoJaPaikka.nuolenPosition;
                nuoli.GetComponent<RectTransform>().localRotation = Quaternion.Euler(vaihe4LaatikonKokoJaPaikka.nuolenRotation);
                //nuoli.GetComponent<RectTransform>().localRotation.SetEulerAngles(vaihe4LaatikonKokoJaPaikka.nuolenRotation);
                seuraavaVaiheNappi.gameObject.SetActive(false);
                ostotiliInput1.interactable = true;
                float pankkitilinArvo2 = 0;
                float.TryParse(ostotiliInput1.text.Replace(",","."),out pankkitilinArvo2);
                Debug.Log("Syötetty arvo pankkitilille on: " + pankkitilinArvo2);
                if (pankkitilinArvo2 == 125.39f) {
                    ostotiliKirjausNappi.interactable = true;
                }
                else if (pankkitilinArvo2 != 125.39) {
                    ostotiliKirjausNappi.interactable = false;
                }
                break;
            case 5:

                selitysTeksti.text = "Kun olet tehnyt molemmat kuvauksen mukaiset kirjaukset niin voit tarkistaa kirjauksesi tällä napilla. Tarkistuksen jälkeen sinulle kerrotaan tehtäväkuvauksen alla oliko kirjaukset oikein vai väärin. Oikeista vastauksista saa pisteitä ja vääristä menettää niitä";
                tutoriaaliLaatikko.GetComponent<RectTransform>().localPosition= vaihe5LaatikonKokoJaPaikka.position;
                tutoriaaliLaatikko.GetComponent<RectTransform>().localScale = vaihe5LaatikonKokoJaPaikka.scale;
                nuoli.GetComponent<RectTransform>().localPosition = vaihe5LaatikonKokoJaPaikka.nuolenPosition;
                nuoli.GetComponent<RectTransform>().localRotation = Quaternion.Euler(vaihe5LaatikonKokoJaPaikka.nuolenRotation);
                tarkistaKirjauksetNappi.interactable = true;
                //nuoli.GetComponent<RectTransform>().localRotation.SetEulerAngles(vaihe5LaatikonKokoJaPaikka.nuolenRotation);
                seuraavaVaiheNappi.gameObject.SetActive(false);
                break;
            case 6:
                selitysTeksti.text = "Pisteesti näet tässä. Pisteet säilyvät kenttien välillä, mutta nollaantuvat mikäli käynnistät pelin uudelleen";
                tutoriaaliLaatikko.GetComponent<RectTransform>().localPosition= vaihe6LaatikonKokoJaPaikka.position;
                tutoriaaliLaatikko.GetComponent<RectTransform>().localScale = vaihe6LaatikonKokoJaPaikka.scale;
                nuoli.GetComponent<RectTransform>().localPosition = vaihe6LaatikonKokoJaPaikka.nuolenPosition;
                nuoli.GetComponent<RectTransform>().localRotation = Quaternion.Euler(vaihe6LaatikonKokoJaPaikka.nuolenRotation);
                //nuoli.GetComponent<RectTransform>().localRotation.SetEulerAngles(vaihe6LaatikonKokoJaPaikka.nuolenRotation);
                seuraavaVaiheNappi.interactable = true;
                break;
            case 7:
                selitysTeksti.text = "Tästä näet monesko tehtävä on menossa ja paljonko tehtäviä on yhteensä";
                tutoriaaliLaatikko.GetComponent<RectTransform>().localPosition= vaihe7LaatikonKokoJaPaikka.position;
                tutoriaaliLaatikko.GetComponent<RectTransform>().localScale = vaihe7LaatikonKokoJaPaikka.scale;
                nuoli.GetComponent<RectTransform>().localPosition = vaihe7LaatikonKokoJaPaikka.nuolenPosition;
                nuoli.GetComponent<RectTransform>().localRotation = Quaternion.Euler(vaihe7LaatikonKokoJaPaikka.nuolenRotation);
                //nuoli.GetComponent<RectTransform>().localRotation.SetEulerAngles(vaihe7LaatikonKokoJaPaikka.nuolenRotation);
                seuraavaVaiheNappi.interactable = true;
                break;
            case 8:
                selitysTeksti.text = "Näillä eväillä eteenpäin! Paina Ok palataksesi päävalikkoon";
                tutoriaaliLaatikko.GetComponent<RectTransform>().localPosition= vaihe7LaatikonKokoJaPaikka.position;
                tutoriaaliLaatikko.GetComponent<RectTransform>().localScale = vaihe7LaatikonKokoJaPaikka.scale;
                nuoli.GetComponent<RectTransform>().localPosition = vaihe7LaatikonKokoJaPaikka.nuolenPosition;
                nuoli.GetComponent<RectTransform>().localRotation = Quaternion.Euler(vaihe7LaatikonKokoJaPaikka.nuolenRotation);
                //nuoli.GetComponent<RectTransform>().localRotation.SetEulerAngles(vaihe7LaatikonKokoJaPaikka.nuolenRotation);
                seuraavaVaiheNappi.interactable = true;
                nuoli.gameObject.SetActive(false);
                break;
            case 9:
                SceneManager.LoadScene(0);
                break;

                
        }
	
	}

    public void SeuraavaVaihde() {
        tutoriaalinVaihe++;
    }

    public void PankkiTiliKirjaus() {
        seuraavaVaiheNappi.gameObject.SetActive(true);
        pankkitiliInput1.interactable = false;
        pankkitiliKirjausNappi.interactable = false;
        SeuraavaVaihde();
    }

    public void OstoTiliKirjaus() {
        ostotiliInput1.interactable = false;
        ostotiliKirjausNappi.interactable = false;
        SeuraavaVaihde();
    }
    public void TarkistaKirjaukset() {
        seuraavaVaiheNappi.gameObject.SetActive(true);
        tarkistaKirjauksetNappi.interactable = false;
        StartCoroutine(NaytaOikeinTeksti());
        SeuraavaVaihde();
    }

    IEnumerator NaytaOikeinTeksti() {
        pisteTeksti.text = "Pisteet: 100";
        oikeinTeksti.text = "Kirjaukset olivat oikein";
        yield return new WaitForSeconds(2.0f);
        oikeinTeksti.text = "";
    }
}
