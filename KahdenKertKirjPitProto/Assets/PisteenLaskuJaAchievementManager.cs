﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PisteenLaskuJaAchievementManager : MonoBehaviour {
    public static PisteenLaskuJaAchievementManager instance;
    public float nykyisetPisteet = 0.0f;

    void Awake() {
        nykyisetPisteet = 0.0f;
        DontDestroyOnLoad(gameObject);
        //Tarkistetaan löytyykö scenestä jo scenecreator, jos löytyy niin tuhotaan tämä, jos ei löydy niin instance on tämä scripti
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void KasvataPisteita() {
        nykyisetPisteet += SceneCreatorScript.instance.pisteKerroin * SceneCreatorScript.instance.pisteetOikeastaVastauksesta;
        GameObject.FindGameObjectWithTag("PisteText").GetComponent<Text>().text = "Pisteesi: " + nykyisetPisteet;
    }

    public void VahennaPisteita() {
        nykyisetPisteet -= SceneCreatorScript.instance.pisteKerroin * SceneCreatorScript.instance.pisteetOikeastaVastauksesta;
        GameObject.FindGameObjectWithTag("PisteText").GetComponent<Text>().text = "Pisteesi: " + nykyisetPisteet;
    }
}
